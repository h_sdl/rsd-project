GDAL==2.4.2
ipython==7.12.0
matplotlib==3.1.1
mistune==0.8.4
numpy==1.17.5
pandas==0.25.3
scikit-learn==0.22.1
scipy==1.4.1
