\include{preamble}

\title{
\normalfont \normalsize
\textsc{École Normale Supérieure Paris Saclay\\
	Remote Sensing Data: from sensor to large-scale geospatial data exploitation } \\
	[10pt]
	\rule{\linewidth}{0.45pt} \\[6pt]
	\huge Détection des changements dans les images RSO polarimétriques bi-temporelles \\
	\rule{\linewidth}{0.45pt}  \\[10pt]
}
\author{Hugues Souchard de Lavoreille \& Julien Jerphanion}
\date{\normalsize Mars 2020}

\begin{document}

\onecolumn

\maketitle
\vspace{-1cm}
\begin{onecolabstract}
    Ce document présente les éléments du projet réalisé dans le cadre du cours \emph{Remote Sensing Data: from sensor to large-scale geospatial data exploitation}. L'objet de ce projet est le développement d'une méthode de détection des changements sur des images polarimétriques proposée dans \\ \cite{paper} puis completée et corrigée dans \cite{thesis}. Cette méthode utilise une représentation spécifique des données issues de deux polarisations différentes. Nous présentons les éléments techniques et théoriques notables de cette méthode et exposons les principaux travaux connexes pour sa calibration et l'étude de sa convergence. L'ensemble des résultats et l'implémentation sont référencés et des pistes pour poursuivre ce travail sont enfin présentées.
\end{onecolabstract}
% \tableofcontents
\noindent

\section{Introduction: définition du problème et présentation des données}
\label{sec:problem_definition}

Les images radar polarimétriques sont des images radar exploitant la polarisation des ondes électromagnétiques utilisées lors de l'acquisition. Une antenne radar peut en effet généralement émettre des ondes ayant une polarisation rectiligne. Si l'on définit deux polarisations orthogonales horizontale \og H \fg~et verticale \og V \fg~et que l'on dispose d'antennes capables d'émettre et de recevoir en l'une ou l'autre des deux polarisations, le système radar peut dès lors analyser comment les rétrodiffuseurs conservent la polarisation émise lorsqu'ils réémettent l'onde ou bien la transforment en sa polarisation orthogonale. Dans ce cadre, on dispose de plus d'information que dans le cas d'une seule polarisation. Dans ce projet, nous travaillerons en \og \emph{dual-pol} \fg, ce qui signifie qu'on émet en une seule polarisation (ici V) et qu'on reçoit en deux polarisations : V et H. Dans notre cas, les images obtenues peuvent donc avoir :

\begin{itemize}
    \item une émission en polarisation verticale et une réception en polarisation verticale, on parle alors d'images \og VV \fg;
    \item une émission en polarisation verticale et une réception en polarisation horizontale, on parle alors d'images \og VH \fg.
\end{itemize}

Dans les deux cas, les pixels des images utilisées portent l'amplitude $|z|$ du nombre complexe reçu $z$, qui est directement proportionnelle à la réflectance du sol sur les surfaces imagées et pour les polarisations d'émission et de réception considérées. On travaille souvent — et c'est le cas ici — sur le carré des amplitudes : $|z|^2$, on parle alors d'images en intensité. \\

Les images à notre disposition étaient de deux types, GRD et SLC. Elles étaient majoritairement issues de zone inondées, comme on le voit par exemple sur la figure \ref{ExempleRedon}. Dans la suite nous présentons la différence entre ces deux types d'images.

\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{Redon1VV}\includegraphics[width=0.5\textwidth]{Redon2VV}\\
\includegraphics[width=0.5\textwidth]{Redon1VH}\includegraphics[width=0.5\textwidth]{Redon2VH}
\caption{Images de la région de Redon avant (première colonne) et après inondation (dernière colonne) pour les polarisations VV (première ligne) et VH (dernière ligne). On observe bien les champs inondés, qui apparaissent plus foncés.}
\label{ExempleRedon}
\end{figure}

\subsection{Images \emph{Ground Range Detected} (GRD)}

Les images \emph{Ground Range Detected} correspondent à des images radar à synthèse d'ouverture (RSO) qui ont été pré-traitées, en général par les agences spatiales -- on pourra se référer à la documentation de l'ESA sur les images du satellite Sentinel-1 \cite{grd_doc}. Le bruit de \emph{speckle} a été atténué à l'aide d'un moyennage multi-vues, puis les images ont été corrigées et augmentées (ajout de métadonnées) grâce à un modèle de la Terre. Ce pré-traitement a pour effet de diminuer leur résolution en comparaison des images SLC (voir infra). Elles comportent directement des valeurs en amplitude et l'information de phase n'est plus présente. \\

Dans la suite, les images que nous allons utiliser sont essentiellement issues du satellite Sentinel-1. La liste de ces images est présentée au tableau \ref{datagrd}.

\begin{table}[H]
    \centering
    \begin{tabular}{|l|c|c|c|c|}
        \hline
        \textsc{Image GRD} & \textsc{Hauteur} (en pixels) & \textsc{Largeur} (en pixels) & \textsc{Date T1} & \textsc{Date T2} \\
        \hline
        \hline
        GuyaneAratai & 1437 & 1879 & 20/01/2017 & 11/10/2017\\
        Sajnam & 769 & 718 & 17/05/2018 & 14/09/2018 \\
        DesMoines & 2143 & 2795 & 05/01/2019 & 05/05/2019 \\
        Bruz & 695 & 850 & 08/11/2019 & 26/12/2019 \\
        Gambie & 1313 & 1750 & 28/01/2018 & 15/07/2018 \\
        Angers & 927 & 1854 & 08/11/2019 & 26/12/2019 \\
        Toulouse & 1109 & 1704 & 04/01/2020 & 28/01/2020\\
        MadridIowa & 1211 & 1304 & 05/01/2019 & 23/04/2019 \\
        Montbel & 554 & 831 & 04/01/2020 & 28/01/2020 \\
        GuyaneFortune & 1437 & 1879 & 20/01/2017 & 29/09/2017 \\
        Redon & 618 & 772 & 08/11/2019 & 26/12/2019\\
        \hline        
    \end{tabular}
    \caption{Images GRD utilisées, dimensions et dates choisies}
    \label{datagrd}
\end{table}

\subsection{Images \emph{Single Look Complex} (SLC)}

Les images \emph{Single Look Complex} sont des images plus résolues mais plus bruitées, avec un nombre complexe encodé sur 16 bits par pixel. L'information de phase y est donc normalement préservée mais nous avons restreint notre étude à l'amplitude des pixels des images SLC fournies (l'information de phase en est donc encore une fois absente). \\

Afin de pouvoir travailler sur les images SLC, il est nécessaire de réduire leur niveau de bruit. Nous avons ainsi procédé tout d'abord à un moyennage local en intensité — c'est-à-dire en espace sur une fenêtre de quelques pixels — pour atténuer le bruit de \emph{speckle} multiplicatif contenu dans celles-ci. Ceci correspond en fait à une opération de moyennage multi-vues (comme pour les images GRD) sauf que nous choisissons ici la taille de la fenêtre de moyennage (le nombre de vues) et donc la perte de résolution et l'intensité du filtrage qui résulte de l'opération. Dans notre étude, les fenêtres utilisées étaient de dimensions $(3,3)$ et de $(4,4)$ pour pouvoir tester la méthode avec des niveaux de filtrage différents. \\

La liste des images SLC à notre disposition est présentée au tableau \ref{dataslc}

\begin{table}[H]
	\centering
    \begin{tabular}{|l|c|c|}
        \hline
        \textsc{Image SLC} & \textsc{Hauteur} (en pixels) & \textsc{Largeur} (en pixels) \\
        \hline
        \hline
        Angouleme & 334 & 667 \\
        DesMoines & 334 & 667 \\
        Fortune & 334 & 667 \\
        MadridIowa & 334 & 667 \\
        Poursac & 334 & 667 \\
        Aratai & 334 & 667 \\
        Dombes.mat & 342 & 1064 \\
        Gravier & 334 & 667 \\
        Niradpur & 342 & 683 \\
        Rochefoucauld & 334 & 667 \\
        \hline
    \end{tabular}
    \caption{Image SLC utilisées et leurs dimensions. Les dates des listes d'images n'ayant pas été fournies avec le jeu de données, on a pris à chaque fois les images associées aux première et dernière dates disponibles}
    \label{dataslc}
\end{table}

\section{L'apport des deux polarisations : le vecteur de changement polarimétrique}

\subsection{Définition du vecteur de changement polarimétrique}
Certaines méthodes de détection automatique de changements sont basées sur une étude des log-ratio des images directement. L'auteur de la méthode que nous analysons ici introduit le concept de \emph{vecteurs de changement polarimétrique} (abrégés PCV pour \emph{Polarimetric Change Vectors}). Ces vecteurs sont définis comme la concaténation des log-ratios joints des deux chaînes de polarisation \og VV \fg\  et \og VH \fg :

\begin{equation}
\begin{cases}
X_{LR} = [X^{VV}_{LR}, X^{VH}_{LR}] \\
X^{VV}_{LR} = \log\frac{X^{VV}_2}{X^{VV}_1} \\
X^{VH}_{LR} = \log\frac{X^{VH}_2}{X^{VH}_1}
\end{cases}
\end{equation}

Dans cette méthode, l'idée est donc de détecter le changement grâce au PCV. L'idée est relativement simple : une zone sans changement est supposée avoir une faible réponse dans les deux polarisations : l'amplitude du PCV sera alors petite. Si cette amplitude est élevée au contraire, on pourra étudier le type de changement qui a mené à cette amplitude élevée.

\subsection{Représentation polaire et méthode}

Afin de réaliser la détection de changement et l'identification des changements, on passe à une \emph{représentation polaire} du PCV qui est plus adaptée :

\begin{equation}
\label{rho}
\rho = \sqrt{(X^{VV}_{LR})^2 + (X^{VH}_{LR})^2} > 0\\
\end{equation}

\begin{equation}
\label{phi}
\phi = \text{arctan} \left( \frac{X^{VV}_{LR}}{X^{VH}_{LR}}\right) \in [0, 2 \pi]
\end{equation}


Cette nouvelle représentation nous permet de voir que si $\rho$ est suffisamment faible, on considèrera qu'il n'y a pas eu de changements (la différence entre les deux images est simplement due au bruit de \emph{speckle}) et dans ce cas l'information d'angle $\phi$ n'est pas significative car la phase est uniformément répartie. En revanche, si l'amplitude $\rho$ est élevée, c'est qu'il y a eu un changement et dans ce cas, l'angle permettra de discriminer ce changement en une ou plusieurs classes, comme indiqué sur voir la figure \ref{polar_repr}. \\

De manière théorique, on peut séparer le plan $\mathbb{R}^2$ en plusieurs secteurs qui correspondent à des types de changements différents. Pour plus d'information, on pourra se référer au chapitre 3 de la thèse \cite{thesis} qui détaille les éléments théoriques et physiques des différents secteurs de cette représentation. \\

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{polar_repr}
	\caption{Segmentation de l'espace des vecteurs de changement en huit secteurs (extrait de \cite{thesis}, p.40)}
	\label{polar_repr}
\end{figure}

\newpage

\subsection{Aperçu de la \emph{pipeline} générale}
En résumé, la méthode procède en deux étapes:
\begin{enumerate}
\item On commence par différencier les PCV associés à des changements des PCV qui sont associés à des zones sans changement (\og non-changement \fg). On note les classes associées $\Omega_{c}$ et $\Omega_{nc}$.
\item Ensuite on sous-classifie les PCV associés au changement, c'est-à-dire ceux de $\Omega_{c}$, en différentes classes notées $(\Omega_{c_k})_{k=1}^K$. Les sous-classes peuvent correspondre par exemple à des zones dont l'humidité a beaucoup augmenté, des zones où de nouveaux bâtiments ont été construits, ou des zones asséchées par exemple.
\end{enumerate}

Un aperçu synthétique du processus complet est donné sur l'image \ref{pipeline}.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{pipeline}
	\caption{Récapitulatif du processus de traitement des images (extrait de \cite{thesis}, p.41)}
	\label{pipeline}
\end{figure}

Ces deux classifications sont effectuées par inférence sur deux modèles grâce à des algorithmes EM que nous allons détailler respectivement dans les deux parties suivantes \ref{detection} et \ref{sous-classification}.

\section{Détection de changement}
\label{detection}

La première étape correspond à la détection de changement. On se propose pour cela de travailler sur l'amplitude $\rho$ du vecteur de changement polarimétrique définie à l'équation \ref{rho}. On fait l'hypothèse que $\rho$ suit un mélange de deux distributions de Nakagami : l'une associée aux pixels qui n'ont pas connu de changement entre les deux images (pixels de non-changement) et l'autre associée aux pixels de changement.

\begin{equation}
\label{melange_1}
\boxed{p(\rho) = P_{nc}\mathcal{K}(\rho; m_{nc}, s_{nc}) + (1-P_{nc})\mathcal{K}(\rho; m_{c},s_{c})}
\end{equation}

\noindent avec :

\begin{itemize}
    \item $P_{nc}$, le \emph{prior} de la classe de non-changement $\Omega_{nc}$ ;
    \item $m_{nc}$ et $m_c$, les paramètres de forme des deux distributions ;
    \item $s_{nc}$ et $s_c$, les paramètres d'étendue des deux distributions.
\end{itemize}

\newpage 
\subsection{Distribution de Nakagami}

On introduit ici la distribution de Nakagami qui sera utile pour la suite. Pour $m>0$ un paramètre dit de forme et $s>0$ un paramètre dit d'étendue, la densité de la distribution de Nakagami est donnée par

\begin{equation}
\mathcal{K}(\rho; m,s) = \frac{2^{m^m}}{\Gamma(m) s^m} \rho^{2m-1} e^{-m\rho^2/s} 
\end{equation}

Cette distribution sert notamment à décrire l'intensité des pixels d'une image RSO d'une région de réflectivité homogène traitée en multi-vues, auquel cas $m = L$ et $s = R$ avec $R$ la réflectivité de la région et $L$ le nombre de vues équivalent. \\

On va cependant voir qu'elle permet également, dans certains cas, de caractériser avec une bonne fiabilité l'amplitude du vecteur de changement polarimétrique sur les zones sans changement et que dans ce cas, ses paramètres seront connus \emph{a priori}.

\subsection{Distribution théorique de non-changement}
\label{section-speckle}
Le choix de prendre un mélange de distributions de Nakagami n'est pas totalement anodin : il est motivé par la modélisation du bruit de \emph{speckle} en intensité par une loi Gamma \\

En effet, si l'on se donne $i \in \{1, ..., N\}$ pixels issus des images acquises par un satellite, si l'on note $(I_i)_{i=1}^n$ les intensités, $(R_i)_{i=1}^n$ leurs réflectivités théoriques, on montre que dans le modèle du bruit multiplicatif, le bruit de \emph{speckle} suit une loi Gamma :

$$S_i \hat{=} \frac{I_i}{R_i} \sim \Gamma\left(k=L, \theta=\frac{1}{L}\right)$$

On note ici $L$ le nombre de vues équivalent. Ce nombre est propre à un capteur  donné et à un type d'images; il est de $1$ pour des images SLC en théorie, et environ de $4.4$ pour les images GRD HR IW de Sentinel-1 comme indiqué sur le site de l'ESA \cite{grd_doc}.

Par conséquent, si la scène n'a pas changé entre deux acquisitions $1$ et $2$, les réflectivités $R_i^{(1)}$ et $R_i^{(2)}$ sont identiques, et le rapport de $I_i^{(1)}$ et $I_i^{(2)}$ est un simple rapport de bruits de \emph{speckle}.

$$\forall i \in \{1, ..., n\} \qquad \frac{I_i^{(1)}}{I_i^{(2)}} = \frac{S_i^{(1)}}{S_i^{(2)}}$$

Il est alors possible, si l'on connaît $L$, de simuler un très grand nombre de tirages de bruits de \emph{speckle} et d'utiliser la formule suivante (découlant directement de la formule (\ref{rho})) pour effectuer un tirage d'amplitudes de vecteurs de changement polarimétrique sur une zone sans changement :

$$R = \sqrt{\log\left(\frac{S_1}{S_2}\right)^2 + \log\left(\frac{S_3}{S_4}\right)^2}  \qquad S_1, S_2, S_3, S_4 \sim \Gamma\left(L, \frac{1}{L}\right)$$

En tirant un grand nombre de ces variables aléatoires $R$, on obtient un histogramme de la distribution de $\rho$ sur les zones de non-changement. Si l'on suppose qu'il s'agit d'une distribution de Nakagami, on peut en estimer les paramètres (par exemple avec le même algorithme que celui que nous construisons dans les paragraphes suivants mais avec un mélange d'une seule Nakagami) et vérifier empiriquement que la distribution est ou non proche d'une loi de Nakagami. On essaie également de trouver la loi Gamma la plus approchante possible par la méthode des moments pour évaluer la pertinence d'utiliser une distribution de Nakagami. Les résultats pour différentes valeurs de $L$ sont présentés à la figure \ref{patatedouce} : en pratique on voit que l'approximation de $\rho$ par une distribution de Nakagami n'est valide que pour un nombre de vues équivalent approximativement plus grand que $3.5-4$. \\

\begin{figure}[h]
\centering
\includegraphics[width=0.5\textwidth]{L_1_2}\includegraphics[width=0.5\textwidth]{L_4_4_2}
\includegraphics[width=0.5\textwidth]{L_16_2}
\caption{Amplitude simulée du vecteur de changement polarimétrique en zone de non-changement pour des nombres de vues équivalents de $1$, $4.4$, $16$ et distributions Gamma et Nakagami approchantes}
\label{patatedouce}
\end{figure}

Cette étude valide l'utilisation d'une loi de Nakagami dans le mélange (\ref{melange_1}) si $L$ est suffisamment élevé. On peut alors déterminer les paramètres de la distribution de Nakagami. Pour les images GRD ($L=4.4$), on a :

\begin{equation}
\label{valeurs-theoriques-non-changement-GRD}
m^{th}_{nc}(L=4.4) \approx 0.95 \qquad s^{th}_{nc}(L=4.4) \approx 1.02
\end{equation}

Pour les images $SLC$, il est nécessaire d'effectuer un traitement multivues pour respecter la condition $L \gtrsim 3.5$. On peut par exemple utiliser une fenêtre de taille $(3,3)$ ou $(4,4)$. Si l'on dispose d'une grande surface uniforme comme un lac, on peut estimer le nombre de vues équivalent, qui sera donné par la relation suivante avant d'estimer les paramètres de la distribution de non-changement.

$$\frac{\sigma_I}{\mathbb{E}\left[I\right]} = \frac{1}{\sqrt{L}}$$

Sinon, il sera aussi possible d'estimer directement les paramètres de la distribution de non-changement si l'on dispose de deux images très stables du même endroit : la distribution de $\rho$ sera très proche d'une distribution de Nakagami de non-changement et il suffira d'estimer ses paramètres (par exemple avec l'algorithme EM présenté dans la section suivante \ref{em1}). \\

Les expérimentations ayant mené à cette caractérisation de la distribution de non-changement sont disponibles dans le notebook \texttt{noise.ipynb} attaché au projet. \\

\subsection{Inférence : algorithme EM}
\label{em1}

L'inférence des cinqs paramètres de ce premier modèle se réalise avec un algorithme EM (Expectation Maximisation) \cite{emalgo}. L'écriture de l'EM mène aux équations \ref{prior_binary}, \ref{s} et \ref{m} présentées dans \cite{thesis}, page 42:

\begin{equation}
\label{prior_binary}
P^{(t+1)}(\omega) = \frac{\sum_{i=1}^{MN} p^{(t)}(\omega|\rho_i)}{MN}
\end{equation}

\begin{equation}
\label{s}
s^{(t+1)} (\omega) = \frac{\sum_{i=1}^{MN} p^{(t)}(\omega|\rho_i) \rho_i^2}{\sum_{i=1}^{MN} p^{(t)}(\omega|\rho_i)}
\end{equation}

\begin{equation}
\label{m}
\sum_{i=1}^{MN} p^{(t)}(\omega|\rho_i) \left[1 - \frac{\rho_i^2}{s^{(t)}} + \log\left(\frac{\rho_i^2}{s^{(t)}}\right) - \psi(m^{(t)}) + \log(m^{(t)}) \right] = 0
\end{equation}

L'algorithme s'implémente facilement, la principale difficulté réside dans le calcul des termes de la suite $(m^{(t)})$, implicitement définis par l'équation \ref{m}. À l'itération $t$, on trouve la valeur de $m^{(t)}$ en cherchant la racine de la fonction $f^{(t)}$ définie comme:

$$
f^{(t)}: m \longmapsto \sum_{i=1}^{MN} p^{(t)}(\omega|\rho_i) \left[1 - \frac{\rho_i^2}{s^{(t)}} + \log\left(\frac{\rho_i^2}{s^{(t)}}\right) - \psi(m) + \log(m) \right]
$$

On se réfèrera à la fonction \texttt{nakagami\_mixture\_em} du fichier \texttt{method.py} pour les détails d'implémentation de l'algorithme. \\

Notons qu'à la suite de l'étude du paragraphe précédent, il peut être judicieux de fixer les paramètres de la distribution de Nakagami pour la zone de non-changement si l'on est certain du nombre de vues équivalent de l'image. On introduit donc dans l'algorithme une option pour forcer celle-ci pour l'inférence. Dans ce cas, seule la seconde distribution et les \emph{priors} (proportions de changement et de non-changement) seront inférés. Ceci permet d'apporter une information \textit{a priori} sur la physique de l'acquisition des images RSO.

\subsection{Étude de stabilité de l'EM}
L'algorithme EM a en général un inconvénient : il optimise un critère qui ne possède en général pas une unique solution. Il peut ainsi, dans certains cas, converger vers des paramètres inattendus. La convergence peut aussi dépendre très fortement des paramètres initiaux $P^{(0)}, m^{(0)}, s^{(0)}$ choisis à l'initialisation de l'algorithme.

\paragraph*{Paramètres initiaux}

Il est très important de bien choisir les paramètres de départ pour éviter une convergence trop lente de l'algorithme, qui peut se faire, dans certains cas extrêmes, en plusieurs milliers d'itérations, itérations qui sont de plus relativement lentes du fait de la taille importante des images. \\

On se propose d'initialiser les paramètres comme suit : 
\begin{equation}
P^{(0)} = (0.9, 0.1) \qquad m^{(0)} = (m_{nc}^{th}, 5) \qquad s^{(0)} = (s_{nc}^{th}, 5)
\end{equation}

Ce choix permet de bien différencier deux classes : une à \emph{prior} important dont on pense qu'elle est concentrée et dont on connaît déjà les paramètres théoriques (classe de non changement) et une à \emph{prior} faible et très dispersée, dont on suppose que les réalisations correspondent aux changements.

\paragraph*{Convergence de l'EM}

Une fois ces paramètres initiaux fixés, on se propose d'étudier le comportement de l'algorithme EM sur des données synthétiques pour connaître l'écart des résultats de l'algorithme aux paramètres réels. En effet, il peut y avoir des cas ambigus dans lesquels l'algorithme que l'on propose donne des résultats non optimaux associés à un maximum local. Le code du fichier \texttt{test\_em\_nakagami.py} est écrit à cette fin. \\

On simule les mélanges de lois de Nakagami suivants pour différents $(x,y,z)$ avant d'exécuter l'algorithme EM initialisé comme indiqué au paragraphe précédent :
$$x\ \mathcal{K}(m_{nc}^{th}, s_{nc}^{th}) + (1-x)\ \mathcal{K}(y, z)$$ 

En sous-entendant leur dépendance en $(x,y,z)$ et en notant $P^*_{nc}, m^*_{nc}, m^*_c, s^*_{nc}, s^*_c$ les vrais paramètres du mélange de Nakagami synthétique et $P^{comp}_{nc}, m^{comp}_{nc}, m^{comp}_c, s^{comp}_{nc}, s^{comp}_c$ les paramètres estimés, on évalue la fonction d'erreur suivante sur une grille :

\begin{equation}
E(x,y,z) = \frac{|P^*_{nc} - P^{comp}_{nc}|}{P^*_{nc}} + \frac{|m^*_{nc} - m^{comp}_{nc}|}{m^*_{nc}} + \frac{|m^*_{c} - m^{comp}_{c}|}{m^*_{c}} + \frac{|s^*_{nc} - s^{comp}_{nc}|}{s^*_{nc}} + \frac{|s^*_{c} - s^{comp}_{c}|}{s^*_{c}} \label{error-em-nakagami}
\end{equation}

L'image en fausses couleurs de la figure \ref{bananeflambee} représente l'erreur $E(x,y,z)$. Le paramètre $x$ figure en ordonnée, le paramètre $y$ figure en abscisse et 3 valeurs réalistes et représentatives (compte tenus des simulations effectuées) du paramètre $z$ ont été testées : $0.5, 2$ et $3.5$ (canaux R, G et B). Plus une zone est noire, plus l'erreur est faible, quel que soit le paramètre $z = s^*_{c}$ choisi. Les zones rouges ont une erreur importante lorsque $z = s_c^* 0.5$ mais faible sinon, etc. En un mot, moins le pixel est blanc, meilleure est la convergence de l'algorithme. \\

Tout d'abord, on observe que l'erreur est presque partout importante lorsque $z=s_c^* = 0.5$ (zones rouges). Dans ce cas, la distribution de changement est très peu étendue et a tendance à se replier sur la distribution de non-changement : il devient difficile de les séparer. Ce cas arrive peu en pratique, on observe presque toujours des distributions de changement d'étendue $z=s_c^* > 1$. Les zones rouges sont donc en pratiques de très bonnes zones de convergence de l'algorithme. De même, on note que l'algorithme converge moins bien lorsque le paramètre $y = m_c^*$ est petit (zone vert-grise) pour la même raison, mais au contraire qu'il converge mieux lorsqu'il n'y a pas de changement du tout (zone noire en bas de l'image, $x = P_{nc} = 1$). En conclusion, on observe en pratique que l'algorithme converge bien pour les valeurs de \emph{priors} étudiées (entre 0 et 30\% de changements) et pour des distributions suffisamment étendues (paramètres $s$ et $m$ pas trop petits), ce qui est plutôt rassurant et conforte notre choix de valeurs initiales pour l'algorithme EM.

\begin{figure}[h]
\centering
\includegraphics[scale=0.9]{error_em_nakagami_v2}
\caption{Erreur de l'algorithme EM, définie à l'équation (\ref{error-em-nakagami})}
\label{bananeflambee}
\end{figure}

\subsection{Classification}
Après avoir exécuté l'algorithme EM, on obtient les 5 paramètres $P_{nc}$, $m_{nc}$, $s_{nc}$, $m_c$, $s_c$ définis à l'équation \ref{melange_1}, ce qui se traduit implicitement par la figure \ref{framboise}.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{Redon_RHO}
\caption{Résultat de l'algorithme EM sur les amplitudes après convergence}
\label{framboise}
\end{figure}

On classifie alors les pixels $(x_i)_i$ de l'image dans les deux classes $\Omega_c$ de changement et $\Omega_{nc}$ de non changement en utilisant la règle de décision de Bayes :

$$
x_i \in \Omega_c \Leftrightarrow p(\Omega_c  | \rho_i) > p(\Omega_{nc}  | \rho_i) \Leftrightarrow P_c\ p(\rho_i | \Omega_c) > \frac{1}{2}
$$

Ceci correspond intuitivement aux zones de la figure \ref{framboise} où la courbe orange est au-dessus de la courbe bleue donc dans ce cas à un simple seuillage sur l'amplitude de $\rho$ aux alentours de $1.8$.

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{vcp2}
\caption{Exemple de classification sur Redon : les pixels jaunes sont associés aux zones sans changement, les pixels bleus et verts sont associés à deux classes de changements.}
\label{vcp2}
\end{figure}


\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{pc2}
\caption{Exemple de classification sur Redon : les pixels jaunes sont associés aux zones sans changement, les pixels bleus et verts sont associés à deux classes de changements.}
\label{resultats_redon}
\end{figure}


\newpage
\section{Sous-classification des changements}
\label{sous-classification}

Après avoir détecté les pixels associés à la classe de changement $\Omega_c$, on peut chercher à réaliser une classification de ces changements en $K$ sous-classes notées $(\omega_{ck})_{k=1}^K$:

$$
\Omega_c = \sqcup_{k=1}^K \omega_{ck}
$$

Pour cela, on s'intéresse cette fois-ci aux angles $(\phi_i)_i$ qui apportent une information sur le type de changement en question comme présenté sur la figure \ref{polar_repr}. \\

Pour ce faire, on pose l'hypothèse que les angles $(\phi_i)_i$ suivent un mélange de distributions normales généralisées:

$$
\boxed{p(\phi) = \sum_{k=1}^K {P_{ck}}\ \mathcal{GN}(\phi;  {\mu_{ck}},  {\alpha_{ck}}, {\beta_{ck}}) \quad \text{s.t} \quad \sum_{k=1}^K P_{ck} = 1}
$$

\noindent où les paramètres à inférer dans ce modèle sont :

\begin{itemize}
    \item $({P_{ck}})_{k=1}^K$, les \emph{priors} des classes de changements ;
    \item $({\mu_{ck}})_{k=1}^K$, les paramètres de localisation des distributions de changements ;
    \item $({\alpha_{ck}})_{k=1}^K$, les paramètres d'étendue des distributions de changements ;
    \item $({\beta_{ck}})_{k=1}^K$, les exposants des distributions de changements.
\end{itemize}

\newpage

\subsection{Distribution normale généralisée}

On donne ici quelques éléments de définition des distributions normales généralisées. Intuitivement, ces distributions peuvent être vues comme une généralisation des distributions normales sur lesquelles on dispose d'un nouveau degré de liberté introduit avec l'exposant $\beta$.  Pour $\mu \in \mathbb{R}$ un paramètre de localisation et $\alpha>0$ un paramètre dit d'étendue, la densité de la distribution normale généralisée est donnée par:

\begin{equation}
\mathcal{GN}(\phi; \alpha, \mu, \beta) = \frac{\beta}{2\alpha \Gamma\left(\beta^{-1}\right)} \exp- \left(\frac{|\phi - \mu|}{\alpha}\right)^\beta
\end{equation}

Cette distribution est utilisée en traitement de l'image et du signal du fait de la flexibilité qu'elle offre pour la modélisation \cite{NacereddineTZH10a}, \cite{AlliliBZ07} \cite{Tarek10}. Dans cette application, elle donne un très bon ajustement aux observations empiriques des angles $(\phi_i)_i$ des vecteurs de changements.


\subsection{Inférence : algorithme EM}

Pour l'inférence des paramètres, on réalise là aussi un algorithme EM (Expectation Maximisation) \cite{emalgo}. L'écriture de l'EM mène aux équations \ref{P2}, \ref{alpha}, \ref{mu} et \ref{beta} présentées dans \cite{thesis}, page 44:

\begin{equation}
\label{P2}
P^{(t)1}(\omega_{ck}) = \frac{\sum_{i=1}^{|\Omega_c|} p^{(t)}(\omega_{ck}|\phi_i)}{|\Omega_c|}
\end{equation}


\begin{equation}
\label{alpha}
\alpha_k^{(t+1)} = \left[ \frac{\beta_k^{(t)} \sum_{i=1}^{|\Omega_c |} p^{(t)}(\omega_{ck}|\phi_i) |\phi_i - \mu_k^{(t)}|^{\beta_k^{(t)}} }{\sum_{i=1}^{|\Omega_c|} p^{(t)}(\omega_{ck} |\phi_i)}\right]^{1 / \beta^{(t)}_k}
\end{equation}


\begin{equation}
\label{mu}
\sum_{i=1}^{|\Omega_c|} p^{(t)}(\omega_{ck}|\phi_i) \frac{\beta_k^{(t)}}{(\alpha_k^{(t)})^{\beta^{(t)}}} | \phi_i - \mu_k^{(t+1)}|^{\beta_k^{(t)}} \ \text{sign}\left(\phi_i - \mu_k^{(t+1)}\right) = 0
\end{equation}


\begin{equation}
\label{beta}
\sum_{i=1}^{|\Omega_c|} p^{(t)}(\omega_{ck}|\phi_i) \left[ \frac{1}{\beta_k^{(t+1)}} + \frac{\psi\left(\frac{1}{\beta_k^{(t+1)}}\right)}{\left(\beta_k^{(t+1)}\right)^2} - \left(\frac{|\phi_i - \mu_k^{(t+1)}|}{(\beta_k^{(t+1)})^2}\right)^{\beta_k^{(t)}} \ \log \left(\frac{|\phi_i - \mu_k^{(t+1)}|}{(\beta_k^{(t+1)})^2}\right)\right] = 0
\end{equation}

On pourra se référer à \cite{Nguyen2014} pour des explications exhaustives sur ce modèle. \\

Après l'inférence et pour la classification, on réalise ensuite le choix de la classe selon la règle de décision de Bayes pour chacun des vecteurs de changement polarimétrique : on assigne à chaque pixel la classe ayant la plus grande probabilité \emph{a posteriori}.\\

\subsubsection{Notes techniques}

On donne ici quelques informations sur l'implémentation de cet algorithme.

\paragraph*{Changement de la plage de valeurs pour les angles}

Si l'on utilise la représentation polaire avec une plage de valeurs de $\phi$ dans $[0, 2\pi]$, il est possible que les modes des distributions soient tronqués, ce qui mène à de mauvais résultats finaux. Théoriquement parlant, on devrait réaliser l'inférence sur des distributions de support homéomorphe à $\mathbb{S}_1$ mais l'étude de l'inférence est relativement plus complexe. On peut néanmoins facilement corriger cela pour obtenir de meilleurs résultats. Pour ce faire, on identifie les valeurs de $\phi$ sur une nouvelle plage:

$$
\phi \in [\tau, \tau + 2\pi]
$$

où $\tau$ est un paramètre réel trouvé empiriquement que l'on obtient en cherchant la boîte d'histogramme regroupant le moins d'observations $\phi_i$. En identifiant les angles inférieurs à $\tau$ par leur semblables dans $[2\pi, \tau + 2\pi[$ cela permet d'obtenir un représentation qui préserve les modes.

\paragraph*{Résolutions numériques}

L'inférence des termes des familles des suites $\{(\beta_k^{(t)})_k\}_t$ et $\{(\mu_k^{(t)})_k\}_t$ est un peu plus complexe car ces termes sont définis implicitement (voir équations \ref{mu}, \ref{beta}). À l'itération $t$, on cherche numériquement les termes $(\beta_k^{(t)})_k$ ;  on peut ensuite en déduire les termes $(\mu_k^{(t)})_k$. \\

Pour cela, on cherche les racines des fonctions $g^{(t)}$ et $h^{(t)}$ définies respectivement par:

\begin{equation}
\label{gt}
g^{(t)}: \mu \longmapsto \sum_{i=1}^{|\Omega_c|} p^{(t - 1)}(\omega_{ck}|\phi_i) \frac{\beta_k^{(t - 1)}}{(\alpha_k^{(t - 1)})^{\beta^{(t - 1)}}} | \phi_i - \mu |^{\beta_k^{(t - 1)}} \ \text{sign}\left(\phi_i - \mu \right) = 0
\end{equation}


\begin{equation}
\label{ht}
h^{(t)}: \beta \longmapsto \sum_{i=1}^{|\Omega_c|} p^{(t)}(\omega_{ck}|\phi_i) \left[ \frac{1}{\beta} + \frac{\psi\left(\frac{1}{\beta}\right)}{\beta^2} - \left(\frac{|\phi_i - \mu_k^{(t+1)}|}{\beta^2}\right)^{\beta_k^{(t)}} \ \log \left(\frac{|\phi_i - \mu_k^{(t+1)}|}{\beta^2}\right)\right] = 0
\end{equation}


Dans notre implémentation, nous utilisons les méthodes d'optimisation \texttt{scipy.optimize.rootscalar} et \texttt{scipy.optimize.fsolve} respectivement sur \eqref{gt} et \eqref{ht}.

\paragraph*{Plage de valeurs forcée pour $\beta$}

L'exposant $\beta$ offre un degré de liberté pour ce modèle. Néanmoins dans l'implémentation, il est possible qu'il y ait une grande perte de stabilité de l'algorithme et que les $\beta$ tendent vers des valeurs infinies. De même, il est empiriquement rare d'obtenir des distributions pour $\beta < 1$ qui soient interprétables. \\

Ainsi, pour plus de stabilité et pour une meilleure interprétation, nous forçons la plage des valeurs des exposants $(\beta_{ck})_{k=1}^K$ à $[1, 3]$.

\paragraph*{Choix du nombre de classes $K$}

Dans les algorithmes de classification non-supervisée, il est d'usage d'utiliser un critère pour choisir le nombre de classes adaptées à la classification. Un des critères canoniquement utilisé est le critère d'information Bayésien (BIC). Dans le cadre de cette méthode, ce critère est défini comme suit:

\begin{equation}
\text{BIC}(K) = (4K - 1) \log |\Omega_c| - 2 \log L(K)
\end{equation}

où $\hat{L}$ est l'estimée par l'algorithme EM de la log-vraisemblance $L$ d'un modèle pour un nombre $K$ de classes:

$$
L(K) = \sum_{i=1}^{|\Omega_c|} \log \left( \sum_{k=1}^K P(\omega_{ck})\ p(\phi_i|\omega_{ck})\right)
$$

\cite{thesis} propose de rechercher $K^*$, le nombre de classes minimisant ce critère pour $K \in [\![1, 8]\!]$ du fait de la présence de huit axes secteurs de changement comme présenté sur la figure \ref{polar_repr}. \\

En pratique, nos expériences et résultats montrent qu'il est rare d'obtenir plus de deux classes de changements. Ce constat est aussi observé dans \cite{thesis}. Ainsi, nous utilisons ce nombre de classes par défaut. Néanmoins, il faudrait réitérer ces expérimentations sur de nouvelles images avec une plus grande variété de changements pour conclure.

\section{Implémentation, jeu de données et résultats}

\subsection{Implémentation}

L'implémentation de la méthode a été réalisé en \emph{Python}, à l'aide de l'écosystème numérique de \emph{scipy} \cite{scipy}, \emph{numpy} \cite{numpy} et \emph{scikit-image} \cite{scikit-image}. Celle-ci est disponible en ligne à l'adresse suivante:

\begin{center}
    \url{https://gitlab.com/jjerphan/rsd-project}
\end{center}

Cette implémentation reprend des éléments de base du code de Gabriele Facciolo\footnote{\url{facciolo@cmla.ens-cachan.fr}}, Carlo de Franchis\footnote{\url{carlo.de-franchis@ens-cachan.fr}} et de Florence Tupin\footnote{\url{florence.tupin@telecom-paris.fr}}.

\subsection{Jeu de données}

Nous disposons de données d'images GRD et d'images SLC, celles-ci ont été présentées plus haut aux tableaux \ref{datagrd} et \ref{dataslc}. Ce jeu de données est disponible à l'adresse suivante : 
\begin{center}
	\url{https://www.dropbox.com/sh/ghc3cqa0y8qi49a/AAAk1JMjiBagS6qwdxRfW_CAa?dl=0}
\end{center}

Les images GRD ont été traitées en forçant la distribution de non-changement avec les paramètres trouvés à l'équation \ref{valeurs-theoriques-non-changement-GRD}, à l'exception des images "\texttt{Toulouse}" et "\texttt{Montbel}" qui, pour une raison encore inexpliquée, correspondaient mal à cette distribution théorique. \\

Les images SLC ont été toutes traitées après moyennage multi-vues avec deux tailles de fenêtre glissante différente : $(3,3)$ et $(4,4)$. En l'absence de surfaces uniformes suffisamment grandes dans les images, nous n'avons pas pu estimer le nombre de vues équivalent $L$ de manière directe. En revanche, les images "\texttt{Gravier}" et "\texttt{Rochefoucauld}" étaient très stables (très peu de changements), ce qui nous a permis d'estimer les paramètres de la distribution de Nakagami de non-changement après moyennage multi-vues (comme expliqué à la fin du paragraphe \ref{section-speckle}). Ces paramètres ont ensuite été forcés pour obtenir les résultats.


\subsection{Résultats obtenus et interprétation physique}

Les résultats de cette méthode sur la représentation des vecteurs de changement sont donnés dans le cas de l'image GRD "\texttt{Redon}" sur la figure \ref{vcp2}. De même, les résultats de la classification finale des pixels sont donnés sur la figure \ref{resultats_redon}.

La classe bleue correspond aux zones inondées, ce qui est théoriquement en phase avec sur la figure \ref{polar_repr}. 
De plus, on remarque la bande de pixels verts en $(170, 630)$ sur la figure \ref{resultats_redon} correspond à un pont. Ces changements sont associés à de nouveaux éléments dans l'image qu'il faudrait inspecter. \\

On donne les résultats pour chaque jeu de données aux adresses suivantes :

\begin{itemize}
	\item Pour les images GRD:
	\begin{center}
		\url{https://cloud.jjerphan.xyz/s/grd-results}
	\end{center}
	
	\item Pour les images SLC traitées en multivues (3,3):
	\begin{center}
		\url{https://cloud.jjerphan.xyz/s/slc9-results}
	\end{center}
	
	\item Pour les images SLC traitées en multivues (4,4):
	\begin{center}
		\url{https://cloud.jjerphan.xyz/s/slc16-results}
	\end{center}
\end{itemize}

\bigskip

\noindent La présentation effectuée en cours à la fin du projet le vendredi 27 mars 2020 est pour sa part disponible à l'adresse:
\begin{center}
	\url{https://cloud.mines-paristech.fr/index.php/s/aXhZ2o5BM8fIBR0}
\end{center}

\section{Conclusion: discussions et ouverture}
\label{sec:conclusion}

    \subsection{Autres réalisations}

    Pour des raisons de concision, nous n'avons présenté que l'essentiel de notre travail dans ce document. Nous avons réalisé d'autres expérimentations listées de manière non exhaustive ici :

    \begin{itemize}
        \item la réalisation de \emph{Jupyter notebooks} pour l'étude de la distribution théorique de non changement ;
        \item l'adaptation de la méthode pour du mono-canal (une seule polarisation) et la comparaison des résultats ;
        \item l'affichage interactif des résultats des algorithmes EM pour inspection ;
        \item l'estimation des paramètres de multi-vues ;
        \item le prétraitement et la correction de certaines images.
    \end{itemize}

    \subsection{Critique succinte de la méthode}

    L'étude de cette méthode nous permet de dresser plusieurs de ses avantages. Cette méthode est relativement simple à comprendre et à implémenter et peu coûteuse en calculs. De même, elle permet par sa conception d'introduire de l'information \emph{a priori} et offre une interprétation physique immédiate des résultats. \\

    Néanmoins, puisque cette méthode travaille au niveau du pixel, elle est très sensible aux décalages des images, même de quelques pixels. De même, cette méthode n'est pas robuste: s'il existe des zones de coupe dues à des sélections géographiques dans les images RSO ou des zones d'ombres dues au capteur, la méthode donne de mauvais résultats mauvais qui sont difficilement interprétables.
    
    \subsection{Perspectives}

    Nous voyons plusieurs perspectives d'amélioration possibles du projet:

    \begin{itemize}
        \item une preuve ou une infirmation de la distribution de Nakagami pour les amplitudes $\rho_i$ des vecteurs de changement polarimétrique ;
        \item la mise en place d'une pipeline générale pour la prise en compte de différents types d'images ; 
        \item le développement d'une interface en ligne de commande et la distribution de l'implémentation sous un paquet \emph{PyPI} pour faciliter son utilisation.
    \end{itemize}


\[ \star \quad \star \quad \star \]


\newpage

\nocite{*}
\bibliographystyle{plainnat}
\bibliography{biblio}


\end{document}
