# -*- coding: utf-8 -*-

import numpy as np
from scipy.stats import rv_continuous
from scipy.special import gammainc
from scipy.special._ufuncs import gamma
from settings import eps

"""
Statistics utilities
* Generalized Nakagami distributions
* Synthetic data generation for testing purposes
"""


def nakagami(rho, m, s):
    """
    Generalized Nakagami probability density function.

    Parameters
    ----------
    rho : np.ndarray
        Number of Nakagamis in the mixture
    m : float
        Shape parameter.
    s : float
        Spread parameter.
    """
    logg = (
        m * np.log(m + eps)
        + np.log(2)
        - np.log(gamma(m))
        - m * np.log(s + eps)
        + (2 * m - 1) * np.log(rho + eps)
        - m * rho ** 2 / (s + eps)
    )
    return np.exp(logg)


class generalized_nakagami(rv_continuous):
    """
    A generalized Nakagami PRNG

    Extends scipy rvs for implementation

    """

    def _cdf(self, x, m, s):
        return 0 if x < 0 else gammainc(m, m / s * x ** 2)


def fast_sample_generalized_nakagamis(m, s, size=None):
    """ For faster generalized draws """
    return np.sqrt(np.random.gamma(m, s / m, size=size))


def draw_random_nakagami_mixture(
    n_components: int,
    n_samples: int,
    P: np.ndarray = None,
    m: np.ndarray = None,
    s: np.ndarray = None,
):
    """
    Draw n_samples samples from a Nakagami mixture with n_components components

    Parameters
    ----------
    n_components : int
        Number of Nakagamis in the mixture
    n_samples : int
        Number of samples to draw
    P : np.ndarray, optional
        Priors to use. The default is None.
    m : np.ndarray, optional
        Shape parameters to use. The default is None.
    s : np.ndarray, optional
        Spread parameters to use. The default is None.

    Returns
    -------
    data : np.ndarray
        Array containing the samples
    P : np.ndarray
        The used priors for the mixture.
    m : np.ndarray
        The used shape parameters for each component in the mixture.
    s : np.ndarray
        The used spread parameters for each component in the mixture.
    """

    if P is None or len(P) != n_components:
        # Random mixture parameters
        P = np.random.random(n_components)
        P /= np.sum(P)

    if m is None or len(m) != n_components:
        m = np.random.gamma(1, size=n_components)

    if s is None or len(s) != n_components:
        s = np.random.gamma(1, size=n_components)

    # Drawing the used nakagami
    k = np.random.random(n_samples)[:, None]
    k = np.argmax(
        np.tile(k, (1, n_components)) - np.tile(np.cumsum(P)[None, :], (n_samples, 1))
        < 0,
        axis=1,
    )

    # Drawing the nakagamis
    data = fast_sample_generalized_nakagamis(m[k], s[k])

    return data, P, m, s
