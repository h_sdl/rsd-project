# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import logging
from method import nakagami_mixture_em
from stats import draw_random_nakagami_mixture
from plots import plot_rho_distribution_interactive

random_state = 42
np.random.seed(random_state)

logging.getLogger().disabled = True

"""
Some tests snippets to test the EM algorithms.
"""


# %%
def test_nakagami_em(
    n_components: int = 2,
    n_samples: int = 20000,
    max_iter: int = 2000,
    random_state: int = 42,
):
    """
    Testing the implemented Nakagami EM with a synthetic Nakagami mixture

    Parameters
    ----------
    n_components : int, optional
        Number of components of the mixture. The default is 2.
    n_samples : int, optional
        Number of sampled to be drawn. The default is 10000.
    max_iter : int, optional
        Maximum number of iterations of the EM. The default is 2000.
    random_state : int, optional
        For reproducibility. The default is 42.

    Returns
    -------
    None.
    """

    np.random.seed(random_state)

    # Initialization for EM
    P0 = np.array([0.9, 0.1])
    m0 = np.array([0.5, 1])
    s0 = np.array([0.5, 5])

    # True parameters
    P = np.array([0.9, 0.1])
    m = np.array([1, 2])
    s = np.array([1, 3])

    rho, P, m, s = draw_random_nakagami_mixture(n_components, n_samples, P, m, s)
    Pcomp, mcomp, scomp, pcomp = nakagami_mixture_em(
        rho, n_components, P0=P0, m0=m0, s0=s0, fixed_prior=True, max_iter=max_iter
    )

    plot_rho_distribution_interactive(rho, Pcomp, mcomp, scomp, P, m, s)

    print("Mixture parameters")
    print("   | Real | Comp |")
    print("------------------")
    print("P0 | %.2f | %.2f |" % (P[0], Pcomp[0, -1]))
    print("P1 | %.2f | %.2f |" % (P[1], Pcomp[1, -1]))
    print("------------------")
    print("m0 | %.2f | %.2f |" % (m[0], mcomp[0, -1]))
    print("m1 | %.2f | %.2f |" % (m[1], mcomp[1, -1]))
    print("------------------")
    print("s0 | %.2f | %.2f |" % (s[0], scomp[0, -1]))
    print("s1 | %.2f | %.2f |" % (s[1], scomp[1, -1]))


def compute_error(x, y, z, plots=False, max_iter=200):
    """
    Computes the error in parameter estimation for a True mixture with parameters
    prior = (x, 1-x)
    shape = (0.95, y)
    spread = (1.02, z)
    """

    P_nc = 0.9
    m_c = 5
    s_c = 5
    m_nc = 0.95
    s_nc = 1.02

    # Initialization for EM
    P0 = np.array([0.9, 0.1])
    m0 = np.array([m_nc, 5])
    s0 = np.array([s_nc, 5])

    # True parameters
    P = np.array([x, 1 - x])
    m = np.array([m_nc, y])
    s = np.array([s_nc, z])

    n_components = 2
    n_samples = 100000

    # Drawing samples
    rho, P, m, s = draw_random_nakagami_mixture(n_components, n_samples, P, m, s)

    # Estimating parameters
    Pcomp, mcomp, scomp, pcomp = nakagami_mixture_em(
        rho,
        P0=np.array([P_nc, 1 - P_nc]),
        m0=np.array([m_nc, m_c]),
        s0=np.array([s_nc, s_c]),
        fixed_nc_distribution=False,
        m_nc=m_nc,
        s_nc=s_nc,
        max_iter=500,
    )

    # Switching between estimated parameters if needed (if we detect that the highest prior is the second one)
    if np.argmin(abs(Pcomp[:, -1] - P[0])) == 1:
        Pcomp = Pcomp[::-1, :]
        scomp = scomp[::-1, :]
        mcomp = mcomp[::-1, :]

    # Plotting if required (debug, etc.)
    if plots:
        plot_rho_distribution_interactive(
            rho, Pcomp, mcomp, scomp, Preal=P, mreal=m, sreal=s
        )

    # Computing relative errors in parameters
    ea = (Pcomp[0, -1] - P[0]) / P[0]
    eb, ec = (mcomp[:, -1] - m) / m, (scomp[:, -1] - s) / s

    # Summing
    return (abs(ea) + sum(abs(eb)) + sum(abs(ec)), ea, eb, ec)


# %%

# Evaluating for priors from 0.7 to 1
xr = np.linspace(0.7, 1, 10)
# Shapes from 0.5 to 15
yr = np.linspace(0.5, 15, 10)
# 3 possible spreads
zr = np.array([0.5, 2, 3.5])

# Storing resulting errors
results = np.zeros((len(xr), len(yr), len(zr)))
for i in range(len(xr)):
    for j in range(len(yr)):
        for k in range(len(zr)):
            print(
                (i * len(yr) * len(zr) + j * len(zr) + k)
                / (len(xr) * len(yr) * len(zr))
                * 100
            )
            x, y, z = xr[i], yr[j], zr[k]
            a, b, c, d = compute_error(x, y, z)
            results[i, j, k] = a

# %%

# for i in range(3):
#     results2[:,:,i] = scipy.interpolate.interp2d(yr, xr, results[:,:,i])(showx, showy)

plt.imshow(
    results[:, :, 2], extent=[yr[:].min(), yr.max(), xr.max(), xr.min()], aspect="auto"
)
plt.colorbar()
plt.imshow(
    results[:, :, :3] / results[:, :, :3].max(),
    extent=[yr[1:].min(), yr.max(), xr.max(), xr.min()],
    aspect="auto",
)
plt.xlabel("Shape parameter of the change distribution")
plt.ylabel("Prior of the non-change distribution")
