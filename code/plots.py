# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
from matplotlib import colors
from scipy.stats import gennorm

from settings import eps, DEFAULT_FIG_SIZE
from stats import nakagami


"""
Plotting tools for:
 * SAR Images visualisation (both interactive and non-interactive)
 * Log-ratio 
 * Histogram and inferred distributions for rho (both interactive and non-interactive)
 * Histogram and inferred distributions for phi (both interactive and non-interactive)
 * Results of the pipeline
"""


def visusar(im, k: int = 3, threshold_min: int = 0):
    """
    Adapted visualisation of SAR images.

    Adapted from mvalab.visusar.

    :param im: 2-channels image
    :param k: multiplicative coefficient for std dev filtering
    :param threshold_min: minimum value for plotting
    """
    im_to_plot = im if np.isrealobj(im) else np.abs(im)

    im_min = np.min(im_to_plot)
    im_max = np.max(im_to_plot)
    im_std = np.std(im_to_plot)
    im_mean = np.mean(im_to_plot)

    threshold_max = im_mean + k * im_std
    if threshold_max > im_max:
        im_to_plot[0, 0] = threshold_max

    masque = im_to_plot < threshold_max
    im_to_plot = im_to_plot * masque + (1 - masque) * threshold_max
    masque2 = im_to_plot > threshold_min
    im_to_plot = im_to_plot * masque2 + (1 - masque2) * threshold_min

    x_label = "Min %.3f   Max %.3f  Mean %.3f   Std %.3f " % (
        im_min,
        im_max,
        im_mean,
        im_std,
    )
    plt.xlabel(x_label)

    plt.imshow(im_to_plot, interpolation="nearest")
    plt.set_cmap("gray")


def visusar_compare(im1, im2, k: int = 3, threshold_min: int = 0):
    """
    Adapted visualisation of SAR images for inspection and comparison.

    Adapted from mvalab.visusar.

    :param im1: first 2-channels image
    :param im2: second 2-channels image
    :param k: multiplicative coefficient for std dev filtering
    :param threshold_min: minimum value for plotting
    :return:
    """
    fig = plt.figure(figsize=(15, 15))
    ax = fig.add_subplot(111)

    n = np.array([0])

    def draw_event():
        plt.cla()
        if n[0] == 0:
            visusar(im1, k, threshold_min)
        else:
            visusar(im2, k, threshold_min)
        plt.draw()

    def press(event):
        if event.key == "right" or event.key == "left":
            n[0] = 1 - n[0]
        draw_event()

    fig.canvas.mpl_connect("key_press_event", press)
    draw_event()
    plt.show(block=False)


def plot_log_ratios(X1, X2, date1, date2, save_path=None, place=""):
    """
    Plot the log ratio of two images.

    :param X1: (h,w,2) np.ndarray
    :param X2: (h,w,2) np.ndarray
    :param date1: first date string
    :param date2: second date string
    :param save_path: where to save
    :param place: name associated to the place depicted by the images
    :return:
    """

    plt.figure(figsize=DEFAULT_FIG_SIZE)

    channels = ["VV channel", "VH channel"]
    for i in range(2):
        plt.subplot(1, 2, i + 1)
        im_to_plot = np.log(1 + X2[:, :, i] / X1[:, :, i])
        plt.title(f"Log ratio on {place} between {date1} and {date2} on {channels[i]} ")

        plt.imshow(im_to_plot, interpolation="nearest")
        plt.set_cmap("gray")

    if save_path is not None:
        plt.savefig(save_path)

    plt.show(block=False)


def plot_pipeline_results(
    X1, X2, X_LR_VV, X_LR_VH, date1, date2, pixel_class, save_path=None, place=""
):
    """
    Plot images between two dates and associated results

    :param X1: (h,w,2) np.ndarray
    :param X2: (h,w,2) np.ndarray
    :param X_LR_VV: (h,w) np.ndarray
    :param X_LR_VH: (h,w) np.ndarray
    :param date1: first date string
    :param date2: second date string
    :param pixel_class: class of pixels
    :param save_path: where to save
    :param place: name associated to the place depicted by the images
    :return:
    """
    h, w, channels = X1.shape

    n_classes = int(pixel_class.max() + 1)

    fig = plt.figure(constrained_layout=True, figsize=DEFAULT_FIG_SIZE)
    gs = fig.add_gridspec(ncols=3, nrows=2)

    # Original images

    x1d1 = fig.add_subplot(gs[0, 0])
    visusar(X1[:, :, 0])
    x1d1.set_title(f"{place} {date1} VV channel")

    x1d2 = fig.add_subplot(gs[0, 1])
    visusar(X1[:, :, 1])
    x1d2.set_title(f"{place} {date1} VH channel")

    x2d1 = fig.add_subplot(gs[-1, 0])
    visusar(X2[:, :, 0])
    x2d1.set_title(f"{place} {date2} VV channel")

    x2d2 = fig.add_subplot(gs[-1, 1])
    visusar(X2[:, :, 1])
    x2d2.set_title(f"{place} {date2} VH channel")

    # Polarimetric changes vectors
    pcv_ax = fig.add_subplot(gs[0, -1])
    cols = ["y", "b", "g", "r", "c", "m", "y", "k"]
    for l in range(n_classes):
        plt.scatter(
            X_LR_VV[pixel_class == l], X_LR_VH[pixel_class == l], alpha=0.2, c=cols[l]
        )
    pcv_ax.set_xlabel(r"$X_{LR}^{VV}$")
    pcv_ax.set_ylabel(r"$X_{LR}^{VH}$")

    min_x_, max_x_ = np.min(X_LR_VV), np.max(X_LR_VV)
    min_y_, max_y_ = np.min(X_LR_VH), np.max(X_LR_VV)

    pcv_ax.axis("equal")
    pcv_ax.axis([min_x_, max_x_, min_y_, max_y_])

    legend = ["Unchanged"] + [f"Changed {l + 1}" for l in range(n_classes)]
    pcv_ax.legend(legend)
    pcv_ax.set_title("PCV")

    # Pixel classification
    pixels_ax = fig.add_subplot(gs[-1, -1])

    pixel_class_rect = pixel_class.reshape((h, w))
    pixels_ax.imshow(pixel_class_rect, cmap=colors.ListedColormap(cols[:n_classes]))
    pixels_ax.set_title("Pixel classification")

    if save_path is not None:
        plt.savefig(save_path)

    plt.show(block=False)


def plot_rho_distributions(
    rho: np.ndarray,
    P: np.ndarray,
    m_star: np.ndarray,
    s_star: np.ndarray,
    save_path=None,
    place="",
):
    """
    Plot the empirical distributions of the rho
    parameters of the Spherical Representation
    of Polarimetric Change Vectors and the inferred ones.

    :param rho: the radius of the PCV
    :param P: prior of the distribution
    :param m_star: the shape parameters for the Nakagami distributions
    :param s_star: the scale parameters for the Nakagami distributions
    :param save_path: where to save
    :param place: name associated to the place depicted by the images
    :return:
    """
    r = np.arange(rho.min(), rho.max(), 0.01) + eps

    # omega_{nc}
    nc = np.array([P[0] * nakagami(x, m_star[0], s_star[0]) for x in r])

    # Omega_c
    c = np.array([P[1] * nakagami(x, m_star[1], s_star[1]) for x in r])

    plt.figure(figsize=DEFAULT_FIG_SIZE)
    plt.plot(
        r,
        nc,
        label=f"Unchanged Nagakami({m_star[0]:.2f}, {s_star[0]:.2f}) ; prior={P[0]:.2f}",
    )
    plt.plot(
        r,
        c,
        label=f"Changed Nagakami({m_star[1]:.2f}, {s_star[1]:.2f}) ; prior={P[1]:.2f}",
    )
    plt.hist(
        rho,
        density=True,
        bins=int(np.sqrt(np.size(rho))),
        alpha=0.5,
        label="Empirical distribution",
    )
    plt.plot(r, nc + c, label=f"Full model (sum)")
    plt.legend()
    plt.title(f"{place} ρ distributions")

    if save_path is not None:
        plt.savefig(save_path)

    plt.show(block=False)


def plot_phi_distributions(
    phi: np.ndarray,
    P: np.ndarray,
    mu: np.ndarray,
    alpha: np.ndarray,
    beta: np.ndarray,
    save_path=None,
    place="",
):
    """
    Plot the empirical distributions of the phi parameters of the Spherical Representation
    of Polarimetric Change Vectors and inferred Generalized Normal distributions.

    :param phi: the angle between the coordinates of the PCV
    :param P: priors of the distributions
    :param alpha: the location parameters for the Generalized Normal distributions
    :param mu: the scale parameters for the Generalized Normal distributions
    :param beta: the exponent for the Generalized Normal distributions
    :param save_path: where to save
    :param place: name associated to the place depicted by the images
    :return:
    """

    K = len(alpha)

    r = np.arange(phi.min(), phi.max(), 0.01) + eps

    plt.figure(figsize=DEFAULT_FIG_SIZE)

    for k in range(K):
        density = [
            P[k] * gennorm.pdf(x, loc=mu[k], scale=alpha[k], beta=beta[k]) for x in r
        ]
        plt.plot(
            r,
            density,
            label=f"Generalized Normal({mu[k]:.2f}, {alpha[k]:.2f}, {beta[k]:.2f}) ; prior={P[k]:.2f}",
        )

    plt.hist(phi, density=True, bins=100, alpha=0.5, label="Empirical distribution")
    plt.legend()
    plt.title(f"{place} φ distributions")

    if save_path is not None:
        plt.savefig(save_path)

    plt.show(block=False)


def plot_nakagami_distribution_with_fit(x: np.ndarray, m: np.ndarray, s: np.ndarray):
    """
    Plot the empirical distribution of a supposed Nakagami alongside with its histogram

    :param x: Nakagami variable
    :param m: the shape parameter for the Nakagami distributions
    :param s: the scale parameter for the Nakagami distributions
    :return:
    """
    r = np.arange(np.quantile(x, 0.001), np.quantile(x, 0.999), 0.01) + eps
    print(r)
    nakagami_r = np.array([nakagami(x, m, s) for x in r])

    plt.figure(figsize=DEFAULT_FIG_SIZE)
    plt.plot(r, nakagami_r, label=f"Nagakami({m:.2f}, {s:.2f})")
    plt.hist(x, density=True, bins=500, alpha=0.5, label="Empirical distribution")
    plt.legend()
    plt.title("X distributions")

    plt.show(block=False)


def plot_rho_distribution_interactive(
    rho, P, m, s, Preal=None, mreal=None, sreal=None, place=""
):
    max_iter = P.shape[1]
    iteration = np.array([0])

    r = np.arange(rho.min(), rho.max(), 0.01) + eps

    fig = plt.figure(figsize=DEFAULT_FIG_SIZE)
    ax = fig.add_subplot(111)

    cref = None
    if Preal is not None and mreal is not None and sreal is not None:
        ncref = np.array([Preal[0] * nakagami(x, mreal[0], sreal[0]) for x in r])
        cref = np.array([Preal[1] * nakagami(x, mreal[1], sreal[1]) for x in r])

    def draw_event():
        # omega_{nc}
        nc = np.array(
            [
                P[0, iteration[0]] * nakagami(x, m[0, iteration[0]], s[0, iteration[0]])
                for x in r
            ]
        )
        # Omega_c
        c = np.array(
            [
                P[1, iteration[0]] * nakagami(x, m[1, iteration[0]], s[1, iteration[0]])
                for x in r
            ]
        )

        plt.cla()
        ax.plot(
            r,
            nc,
            label=f"Unchanged Nagakami({m[0, iteration[0]]:.2f}, {s[0, iteration[0]]:.2f}) ;"
            f"prior={P[0, iteration[0]]:.2f}",
        )
        ax.plot(
            r,
            c,
            label=f"Changed Nagakami({m[1, iteration[0]]:.2f}, {s[1, iteration[0]]:.2f}) ;"
            f"prior={P[1, iteration[0]]:.2f}",
        )
        if cref is not None:
            ax.plot(
                r,
                ncref,
                label=f"Unchanged Nagakami ref({mreal[0]:.2f}, {sreal[0]:.2f}) ; prior={Preal[0]:.2f}",
            )
            ax.plot(
                r,
                cref,
                label=f"Changed Nagakami ref({mreal[1]:.2f}, {sreal[1]:.2f}) ; prior={Preal[1]:.2f}",
            )
        ax.hist(
            rho,
            density=True,
            bins=int(np.sqrt(np.size(rho))),
            alpha=0.5,
            label="Empirical distribution",
        )
        ax.plot(r, nc + c, label=f"Full model (sum)")
        plt.legend()
        plt.title(f"{place} Rho distributions (iteration {iteration[0]})")

        plt.draw()

    def press(event):
        if event.key == "right":
            iteration[0] = min(max_iter - 1, iteration[0] + 1)
        if event.key == "up":
            iteration[0] = min(max_iter - 1, iteration[0] + 10)
        if event.key == "left":
            iteration[0] = max(0, iteration[0] - 1)
        if event.key == "down":
            iteration[0] = max(0, iteration[0] - 10)
        if event.key == "d":
            iteration[0] = min(max_iter - 1, iteration[0] + 100)
        if event.key == "q":
            iteration[0] = max(0, iteration[0] - 100)
        draw_event()

    fig.canvas.mpl_connect("key_press_event", press)
    draw_event()
    plt.show(block=False)


def plot_phi_distribution_interactive(phi, P, mu, alpha, beta, place=""):
    max_iter = P.shape[1]
    iteration = np.array([0])
    K = len(alpha)
    r = np.arange(phi.min(), phi.max(), 0.01) + eps

    fig = plt.figure(figsize=DEFAULT_FIG_SIZE)
    ax = fig.add_subplot(111)

    def draw_event():
        plt.cla()
        for k in range(K):
            density = [
                P[k, iteration[0]]
                * gennorm.pdf(
                    x,
                    loc=mu[k, iteration[0]],
                    scale=alpha[k, iteration[0]],
                    beta=beta[k, iteration[0]],
                )
                for x in r
            ]
            plt.plot(r, density)

        plt.hist(phi, density=True, bins=100, alpha=0.5, label="Empirical distribution")
        plt.legend()
        plt.title(f"{place} phi distributions")

        plt.draw()

    def press(event):
        if event.key == "right":
            iteration[0] = min(max_iter - 1, iteration[0] + 1)
        if event.key == "up":
            iteration[0] = min(max_iter - 1, iteration[0] + 10)
        if event.key == "left":
            iteration[0] = max(0, iteration[0] - 1)
        if event.key == "down":
            iteration[0] = max(0, iteration[0] - 10)
        if event.key == "d":
            iteration[0] = min(max_iter - 1, iteration[0] + 100)
        if event.key == "q":
            iteration[0] = max(0, iteration[0] - 100)
        draw_event()

    fig.canvas.mpl_connect("key_press_event", press)
    draw_event()
    plt.show(block=False)
