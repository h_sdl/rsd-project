#!/usr/bin/env python
# coding: utf-8
import logging
import os

from data import load_grd_data, load_slc_data
from method import (
    estimate_nonchange_nakagami_parameters,
    detection_classification_pipeline,
)
from settings import (
    nb_looks_SLC,
    eq_nb_looks_GRD,
    GRD_FIGS_FOLDER,
    SLC_FIGS_FOLDER,
    FIGS_FOLDER,
)

logging.basicConfig(level=logging.INFO)


"""
Script to run the pipeline on the given GRD and SLC images of the project.
 * Parameters of the non-change distribution are estimated for each type of images
 * The pipeline is run on the GRD images
 * The pipeline is run on the SLC images
 * Result are saved in their associated folder.
 
See settings.py
"""

if __name__ == "__main__":

    grd_places = [
        "GuyaneAratai",
        "Sajnam",
        "DesMoines",
        "Bruz",
        "Gambie",
        "Angers",
        "Toulouse",
        "MadridIowa",
        "Montbel",
        "Toulouse",
        "MadridIowa",
        "Montbel",
        "GuyaneFortune",
        "Redon",
    ]

    slc_places = [
        "Angouleme.mat",
        "DesMoines.mat",
        "Fortune.mat",
        "MadridIowa.mat",
        "Poursac.mat",
        "Aratai.mat",
        "Dombes.mat",
        "Gravier.mat",
        "Niradpur.mat",
        "Rochefoucauld.mat",
    ]
    m_SLC, s_SLC, stdm_SLC, stds_SLC = estimate_nonchange_nakagami_parameters(
        nb_looks_SLC, ENL=False
    )
    logging.info("→ Computed non-change Nakagami parameters for SLC images")
    logging.info(
        f" SLC : L = {nb_looks_SLC}; m = {m_SLC:.2f} ± {stdm_SLC:.2f}; s={s_SLC:.2f} ± {stdm_SLC:.2f}"
    )

    m_GRD, s_GRD, stdm_GRD, stds_GRD = estimate_nonchange_nakagami_parameters(
        eq_nb_looks_GRD
    )
    logging.info("→ Computed non-change Nakagami parameters for GRD images")
    logging.info(
        f" GRD : L = {eq_nb_looks_GRD}; m = {m_GRD:.2f} ± {stdm_GRD:.2f}; s={s_GRD:.2f} ± {stdm_GRD:.2f}"
    )

    load_func_places_grd = [
        (place, load_grd_data, m_GRD, s_GRD, GRD_FIGS_FOLDER) for place in grd_places
    ]

    load_func_places_slc = [
        (
            place,
            lambda im: load_slc_data(im, nb_looks_SLC),
            m_SLC,
            s_SLC,
            SLC_FIGS_FOLDER,
        )
        for place in slc_places
    ]

    os.makedirs(FIGS_FOLDER, exist_ok=True)
    os.makedirs(GRD_FIGS_FOLDER, exist_ok=True)
    os.makedirs(SLC_FIGS_FOLDER, exist_ok=True)

    load_func_places = load_func_places_grd + load_func_places_slc

    for place, load_data_func, m, s, figs_folder in load_func_places:
        logging.info(f"→ Run pipeline on {place}")

        logging.info(f"   → Load data")
        X1, X2, date1, date2 = load_data_func(place)

        detection_classification_pipeline(
            X1,
            X2,
            date1,
            date2,
            m_nc=m,
            s_nc=s,
            fixed_nc_distribution=True,
            place=place,
            figs_folder=figs_folder,
        )
