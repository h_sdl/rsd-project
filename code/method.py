import logging

import numpy as np
from matplotlib import pyplot as plt
from scipy.optimize import root_scalar, fsolve
from scipy.special import digamma
from scipy.stats import gennorm

from plots import (
    plot_log_ratios,
    plot_rho_distributions,
    plot_phi_distributions,
    plot_pipeline_results,
)

from settings import eps, ROOT, FIGS_FOLDER
from stats import nakagami

logging.basicConfig(level=logging.INFO)

"""
* Estimation of non change distribution parameters
* EM algorithm for change detection
* EM algorithm for change classification
"""


def estimate_nonchange_nakagami_parameters(
    L: float = 1,
    n_estimates: int = 10000,
    n_pass: int = 100,
    use_intensity: bool = True,
    ENL: bool = True,
) -> (float, float, float, float):
    """
    Estimates the parameters of the non-change Nakagami for a given number of looks

    Parameters
    ----------
    L : float, optional
        Number of looks. If not set, we suppose the image is SLC
    n_estimates : int, optional
        Number of speckle samples used to estimate m and s in each pass
    n_pass : int, optional
        Number of estimates in total
    use_intensity : bool, optional
        If False, we work in amplitude images
    ENL : bool, optional
        If True, we consider that we work with the ENL
        If False, this is a real L, we do not know for now how to find the ENL in general...

    Returns
    -------
    (m, s, std_m, std_s)
        estimates of m and s and uncertainties in the estimation (standard deviation)

    """

    if not ENL:
        if L == 9:
            return estimate_nonchange_nakagami_parameters(3.95)
        elif L == 16:
            return estimate_nonchange_nakagami_parameters(5.65)
        raise NotImplementedError()

    sim_speckle = np.random.gamma(L, 1 / L, size=(n_estimates, 4, n_pass))
    if not use_intensity:
        sim_speckle = np.sqrt(sim_speckle)

    sim_rho = np.sqrt(
        np.log(sim_speckle[:, 0, :] / sim_speckle[:, 1, :]) ** 2
        + np.log(sim_speckle[:, 2, :] / sim_speckle[:, 3, :]) ** 2
    )

    estimates_m = np.ones(n_pass)
    estimates_s = np.ones(n_pass)

    for k in range(n_pass):
        _, m, s, _ = nakagami_mixture_em(
            sim_rho[:, k],
            n_class=1,
            max_iter=1000,
            tol=1e-8,
            random_state=42,
            verbose=False,
        )
        estimates_m[k] = m[:, -1]
        estimates_s[k] = s[:, -1]

    return (
        np.mean(estimates_m),
        np.mean(estimates_s),
        np.std(estimates_m),
        np.std(estimates_s),
    )


def nakagami_mixture_em(
    rho: np.ndarray,
    n_class: int = 2,
    P0: np.ndarray = None,
    m0: np.ndarray = None,
    s0: np.ndarray = None,
    fixed_nc_distribution: bool = False,
    m_nc: float = 1.0,
    s_nc: float = 0.3,
    max_iter: int = 500,
    tol=1e-8,
    random_state: int = 42,
    verbose: bool = True,
) -> (np.ndarray, np.ndarray, np.ndarray, np.ndarray):
    """
    Nakagami Mixture EM.

    Used to classify PCV between the class associated to unchanged pixels
    and the class associated to changed pixels.

    :param rho: the observed radius of PCV
    :param n_class: the number of class to used, by default 2
    :param P0: initial value of the prior
    :param m0: initial value of the shape parameter
    :param s0: initial value of the spread parameter
    :param fixed_nc_distribution: tells whether the non-change distribution should be enforced along the iterations
    :param m_nc: shape parameter associated to the non-change distribution, used if fixed_nc_distribution set true
    :param s_nc: spread parameter associated to the non-change distribution, used if fixed_nc_distribution set true
    :param max_iter: the maximum number of iterations to perform
    :param tol: convergence tolerance of the EM
    :param random_state: a random state for reproducibility
    :param verbose: if True, logging is done in the function
    :return: Priors, shape parameters, spread parameters and posteriors
    """
    np.random.seed(random_state)

    rho = np.array(rho)

    # N × M here
    n = len(rho)

    P = np.zeros((n_class, max_iter))
    s = np.zeros((n_class, max_iter))
    m = np.zeros((n_class, max_iter))

    # Posteriors (p(⍵|ρ_i))_i
    p = np.zeros((n, n_class))

    # Initialization: it is hard to tune
    if P0 is None:
        P[:, 0] = 1 / n_class
    else:
        if n_class != len(P0):
            raise Exception("Wrong shape for P0")
        P[:, 0] = P0
    if m0 is None:
        m[:, 0] = 5
    else:
        if n_class != len(m0):
            raise Exception("Wrong shape for m0")
        m[:, 0] = m0
    if s0 is None:
        s[:, 0] = 10
    else:
        if n_class != len(s0):
            raise Exception("Wrong shape for s0")
        s[:, 0] = s0

    # To find m that is implicitly defined using root_scalar
    def f(m, const):
        return digamma(m) - np.log(m) - const

    def stopping_criterion(P, m, s, t):
        """
        We stop if parameters haven't moved.
        """
        d_P = np.abs(P[:, t] - P[:, t + 1]).mean()
        d_m = np.abs(m[:, t] - m[:, t + 1]).mean()
        d_s = np.abs(s[:, t] - s[:, t + 1]).mean()

        return d_P < tol and d_m < tol and d_s < tol

    for t in range(max_iter - 1):
        if verbose:
            logging.info(f" Change Detection EM: Iteration {t}")
            logging.info(f"        P = {P[:, t]}")
            logging.info(f"        m = {m[:, t]}")
            logging.info(f"        s = {s[:, t]}")

        # E step
        # Computing the posterior distribution of omega on each data point under
        # current parameters P, m, s using Bayes formula
        for k in range(n_class):
            p[:, k] = nakagami(rho, m[k, t], s[k, t]) * P[k, t]
        norm_p = np.tile(p.sum(axis=1), (n_class, 1)).T
        p /= norm_p + eps

        # M step Updating the parameters for next iteration
        # 1. Mixture parameters
        P[:, t + 1] = p.sum(axis=0) / n

        # Note: we put a normalization step here in the case of error
        # propagation on the normalisation of posterior p
        P[:, t + 1] /= P[:, t + 1].sum()

        # 2. Shape
        for k in range(n_class):
            s[k, t + 1] = (p[:, k] * rho ** 2).sum() / p[:, k].sum()

        # 3. Spread
        for k in range(n_class):
            rho2_on_s = rho ** 2 / (s[k, t] + eps)
            const = (p[:, k] * (1 - rho2_on_s + np.log(rho2_on_s + eps))).sum() / p[
                :, k
            ].sum()
            roots = root_scalar(f, args=const, bracket=[eps, 100]).root
            m[k, t + 1] = roots

        # Enforcing non-change distribution
        if fixed_nc_distribution:
            m[0, t + 1] = m_nc
            s[0, t + 1] = s_nc

        if stopping_criterion(P, m, s, t):
            if verbose:
                logging.info(f"     Converged after {t + 1} iterations")
            break

    # We only keep values of the iterations that ran
    P = P[:, :t]
    m = m[:, :t]
    s = s[:, :t]

    # Sorting on the shape parameter to have the 'nc' class parameters first
    # We assume that the smaller the spread parameter, the less variability we have
    # which can reasonably correspond to no change
    if not (fixed_nc_distribution):
        indices = np.argsort(m[:, -1])
    else:
        indices = np.array([0, 1])
    P = P[indices, :]
    m = m[indices, :]
    s = s[indices, :]
    p = p[:, indices]
    return P, m, s, p


def multiclass_em(
    phi: np.ndarray,
    n_class: int,
    P0: np.ndarray = None,
    mu0: np.ndarray = None,
    alpha0: np.ndarray = None,
    beta0: np.ndarray = None,
    beta_min: float = 1,
    beta_max: float = 3,
    max_iter: int = 500,
    tol: float = 1e-8,
    random_state: int = 42,
) -> (np.ndarray, np.ndarray, np.ndarray, np.ndarray):
    """
    Generalized Gaussian Mixture EM.

    Used to sub-classify PCV in the class associated to changed pixels.

    :param phi: the observed angle between components of PCV
    :param n_class: the number of class to used, by default 2
    :param P0: initial value of the prior
    :param mu0: initial value of the loc parameters
    :param alpha0: initial value of the scale parameters
    :param beta0: initial value of the exponent parameters
    :param beta_min: lower bound for the exponent parameters
    :param beta_max: upper bound for the exponent parameters
    :param max_iter: the maximum number of iterations to perform
    :param tol: convergence tolerance of the EM
    :param random_state: a random state for reproducibility
    :return: Priors, loc parameter, scale parameters, exponent parameters and posteriors
    """

    np.random.seed(random_state)

    phi = np.array(phi)

    # N × M here
    n = len(phi)

    P = np.zeros((n_class, max_iter))
    mu = np.zeros((n_class, max_iter))
    alpha = np.zeros((n_class, max_iter))
    beta = np.zeros((n_class, max_iter))

    # For the optimisation step
    mu_min, mu_max = phi.min(), phi.max()

    # Posteriors (p(⍵|ρ_i))_i
    p = np.zeros((n, n_class))

    # Initialization: it is hard to tune
    if P0 is None:
        P[:, 0] = 1 / n_class
    else:
        if n_class != len(P0):
            raise Exception("Wrong shape for P0")
        P[:, 0] = P0

    if mu0 is None:
        mu[:, 0] = np.linspace(mu_min, mu_max, n_class)
    else:
        if n_class != len(mu0):
            raise Exception("Wrong shape for mu0")
        mu[:, 0] = mu0

    if alpha0 is None:
        alpha[:, 0] = 1  # Standard case (std dev.)
    else:
        if n_class != len(alpha0):
            raise Exception("Wrong shape for alpha0")
        alpha[:, 0] = alpha0

    if beta0 is None:
        beta[:, 0] = 2  # Standard Gaussian
    else:
        if n_class != len(beta0):
            raise Exception("Wrong shape for beta0")
        beta[:, 0] = beta0

    def f_beta(beta, phi, alpha, mu, p):
        """
        Function to find beta that is implicitly defined in (3.28) using fsolve

        :param beta: parameter to optimize
        :param phi: vectors of observation
        :param alpha: scalar
        :param mu: scalar
        :param p: vectors of posterior
        :return:
        """
        Tzkq = np.abs(phi - mu) / alpha

        beta_e = beta.clip(eps, None)
        return (
            p
            * (
                1 / beta_e
                + digamma(1 / beta) / beta_e ** 2
                - Tzkq ** beta_e * np.log(Tzkq + eps)
            )
        ).sum()

    def f_mu(mu, phi, alpha, beta, p):
        """
        Function to find mu that is implicitly defined in (3.27) using root_scalar

        :param mu: parameter to optimize
        :param phi: vectors of observation
        :param alpha: scalar
        :param mu: scalar
        :param p: vectors of posterior
        :return:
        """
        Szkq = phi - mu

        return (
            p
            * (beta / alpha ** beta * np.abs(Szkq) ** (beta - 1 + eps) * np.sign(Szkq))
        ).sum()

    def stopping_criterion(P, mu, alpha, beta, t):
        """
        We stop if parameters haven't moved.
        """
        d_P = np.abs(P[:, t] - P[:, t + 1]).mean()
        d_mu = np.abs(mu[:, t] - mu[:, t + 1]).mean()
        d_alpha = np.abs(alpha[:, t] - alpha[:, t + 1]).mean()
        d_beta = np.abs(beta[:, t] - beta[:, t + 1]).mean()

        return d_P < tol and d_mu < tol and d_alpha < tol and d_beta < tol

    # Start of Algo
    for t in range(max_iter - 1):
        logging.info(f" Change Classification EM: Iteration {t}")
        logging.info(f"        P = {P[:, t]}")
        logging.info(f"        μ = {mu[:, t]}")
        logging.info(f"        α = {alpha[:, t]}")
        logging.info(f"        β = {beta[:, t]}")

        # E step
        # Computing the posterior distribution of omega on each data point under
        # current parameters P, m, s using Bayes formula
        for k in range(n_class):
            p[:, k] = (
                gennorm.pdf(phi, loc=mu[k, t], scale=alpha[k, t], beta=beta[k, t])
                * P[k, t]
            )
        norm_p = np.tile(p.sum(axis=1), (n_class, 1)).T
        p /= norm_p + eps

        # M step Updating the parameters for next iteration
        # 1. Mixture parameters
        P[:, t + 1] = p.sum(axis=0) / n

        # 2. Scale
        for k in range(n_class):
            alpha[k, t + 1] = (
                beta[k, t]
                * (p[:, k] * np.abs(phi - mu[k, t]) ** beta[k, t]).sum()
                / p[:, k].sum()
            )
            alpha[k, t + 1] = alpha[k, t + 1] ** (1 / beta[k, t])

        # 3. Exponent (beta)
        for k in range(n_class):
            beta[k, t + 1] = fsolve(
                f_beta, x0=beta[k, t], args=(phi, alpha[k, t], mu[k, t], p[:, k])
            )
            logging.debug("DEBUG: Computed β[{k},{t+1}] = {beta[k, t + 1]}")
            # We clip beta here to have both meaningful values and stability
            beta[k, t + 1] = beta[k, t + 1].clip(beta_min, beta_max)

        # 3. Loc
        for k in range(n_class):
            mu[k, t + 1] = root_scalar(
                f_mu,
                args=(phi, alpha[k, t], beta[k, t], p[:, k]),
                bracket=[mu_min, mu_max],
            ).root

        if stopping_criterion(P, mu, alpha, beta, t):
            logging.info(f"     Converged after {t + 1} iterations")
            break

    # We only keep values of the iterations that ran
    P = P[:, :t]
    mu = mu[:, :t]
    alpha = alpha[:, :t]
    beta = beta[:, :t]

    # Sorting on the loc parameter first
    indices = np.argsort(mu[:, -1])
    P = P[indices, :]
    mu = mu[indices, :]
    alpha = alpha[indices, :]
    beta = beta[indices, :]
    p = p[:, indices]

    return P, mu, alpha, beta, p


def detection_classification_pipeline(
    X1: np.ndarray,
    X2: np.ndarray,
    date1: str,
    date2: str,
    n_class_changes: int = 2,
    s_c: float = 5,
    m_c: float = 5,
    P_nc=0.9,
    fixed_nc_distribution=False,
    m_nc: float = None,
    s_nc: float = None,
    max_iter: int = 500,
    figs_folder=FIGS_FOLDER,
    place="",
):
    """
    Perform the change detection and classification between two
    dual pols images as described in [2].

    First classify the pixels as associated to change or not then
    classify the changed pixels in `n_class_changes` subclasses.

    Log-ratios, distributions and results are shown and saved.


    [2] Advanced Methods for Change Detection in Multi-polarization and
    Very-High Resolution Multitemporal SAR Images.
    Davide Pirrone. PhD thesis, International Doctorate School in
    Information and Communication Technologies - University of Trento, 1 2019.

    :param X1: first image (2 channels)
    :param X2: second image (2 channels)
    :param date1: first date
    :param date2: second date
    :param n_class_changes: the number of class to identify for the change
    :param s_c: the spread parameter of the change-distribution
    :param m_c: the shape parameter of the change-distribution
    :param fixed_nc_distribution: fix the theoretical distribution of non-change
    :param P_nc: the prior to use for the non-change distribution
    :param m_nc: the theoretical shape parameter of the non-change distribution
    :param s_nc: the theoretical spread parameter of the non-change distribution
    :param max_iter: maximum iteration for both EM algorithms
    :param figs_folder: folder used to save figures
    :param place: name associated to the place depicted by the images
    :return:
    """
    logging.info(f"   → Image 1 date: {date1}")
    logging.info(f"   → Image 2 date: {date2}")

    logging.info(f"   → Plot log ratio")
    save_path = ROOT / "report" / "figs" / f"{place}_log_ratios.png"
    plot_log_ratios(X1, X2, date1, date2, save_path=save_path, place=place)

    # Compute Polarimetric Change Vector
    X_LR = np.log(X2 ** 2 + eps) - np.log(X1 ** 2 + eps)
    X_LR_VV = np.ndarray.flatten(X_LR[:, :, 0])
    X_LR_VH = np.ndarray.flatten(X_LR[:, :, 1])

    # Polar coordinates
    rho = np.sqrt(X_LR_VV ** 2 + X_LR_VH ** 2)
    phi = np.arctan2(X_LR_VH, (X_LR_VV + eps))

    # Binary clustering
    logging.info(f"   → Binary Classification")
    P_binary, m, s, p_binary = nakagami_mixture_em(
        rho,
        P0=np.array([P_nc, 1 - P_nc]),
        m0=np.array([m_nc, m_c]),
        s0=np.array([s_nc, s_c]),
        fixed_nc_distribution=fixed_nc_distribution,
        m_nc=m_nc,
        s_nc=s_nc,
        max_iter=max_iter,
    )
    P_binary_star, m_star, s_star = P_binary[:, -1], m[:, -1], s[:, -1]
    logging.info(f"   → Plot ρ distributions")
    save_path = figs_folder / f"{place}_distrib_rho.png"
    plot_rho_distributions(
        rho, P_binary_star, m_star, s_star, save_path=save_path, place=place
    )

    # Multi-class classification
    class_indices = np.zeros(X_LR_VV.shape)

    # We apply Bayes rules here
    changed_pixels_index = p_binary[:, 0] <= 0.5
    if changed_pixels_index.sum() == 0:
        logging.info(f"   → No change detected!")
    else:
        logging.info(f"   → Multi-Class Classification")
        phi_changed = phi[changed_pixels_index]

        # We shift the representation of phi to get a representation where
        # modes are the best represented. This can be done simply by finding
        # an angle of cut of minimal frequency of occurrences.
        # We then identify all the phi inferiors to this angle by their "+2π"-counterpart
        freqs, _ = np.histogram(phi_changed, bins=int(np.sqrt(len(phi_changed))))

        freq_of_lowest_occurence = (
            np.argmin(freqs) / float(len(freqs)) * 2 * np.pi - np.pi
        )
        phi_changed[phi_changed < freq_of_lowest_occurence] += 2 * np.pi
        logging.info(
            f"   → Shift φ representation to [{freq_of_lowest_occurence}, {freq_of_lowest_occurence} + 2*pi]"
        )

        P_multi, mu, alpha, beta, p_multi = multiclass_em(
            phi_changed, n_class=n_class_changes, max_iter=max_iter
        )
        P_multi_star, mu_star, alpha_star, beta_star = (
            P_multi[:, -1],
            mu[:, -1],
            alpha[:, -1],
            beta[:, -1],
        )

        # 0: no change, 1: first class of changes,, 2: second class of changes
        class_indices[changed_pixels_index] = p_multi.argmax(axis=1) + 1

        logging.info(f"   → Plot φ distributions")
        save_path = figs_folder / f"{place}_distrib_phi.png"
        plot_phi_distributions(
            phi_changed,
            P_multi_star,
            mu_star,
            alpha_star,
            beta_star,
            save_path=save_path,
            place=place,
        )

    logging.info(f"   → Plot Results")
    save_path = figs_folder / f"{place}_res.png"

    plot_pipeline_results(
        X1,
        X2,
        X_LR_VV,
        X_LR_VH,
        date1,
        date2,
        class_indices,
        save_path=save_path,
        place=place,
    )

    plt.close("all")
