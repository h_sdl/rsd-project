# -*- coding: utf-8 -*-
"""
Created on Thu Feb 27 22:43:36 2020
"""

import numpy as np

from method import nakagami_mixture_em
from plots import (
    plot_phi_distributions,
    plot_rho_distributions,
    plot_pipeline_results,
    plot_nakagami_distribution_with_fit,
    plot_rho_distribution_interactive,
)
from data import load_grd_data  # , load_slc_data
import matplotlib.pyplot as plt

import logging
import scipy.stats

logging.basicConfig(level=logging.INFO)


place = "Redon"

X1, X2, date1, date2 = load_grd_data(place)

a = np.ravel(X2[:, :, 0])
b, c, d, e = nakagami_mixture_em(a, n_class=1, fixed_prior=True, P0=[1])
print(b[:, -1], c[:, -1], d[:, -1])


# Amplitude
a = np.ravel(X1[:, :, 0])
a += np.random.random(a.shape) - 0.5  # TODO ...
a = a[a < np.quantile(a, 0.999)]
PX1, mX1, sX1, pX1 = nakagami_mixture_em(a, n_class=1)
plot_nakagami_distribution_with_fit(a, mX1[0, -1], sX1[0, -1])

# Intensity
a = np.ravel(X1[:, :, 0])
a += np.random.random(a.shape) - 0.5  # TODO ...
a = a ** 2 / 1000  # TODO Check that rescaling is OK for Nakagamis
a = a[a < np.quantile(a, 0.999)]
plt.hist(a, bins=700)
PX1, mX1, sX1, pX1 = nakagami_mixture_em(a, n_class=1)
plot_nakagami_distribution_with_fit(a, mX1[0, -1], sX1[0, -1])
# TODO Mauvais fit de Nakagami pour amplitude X1[:,:,1]
# TODO Mauvais fit en intensité

# Rho
X_LR = np.log(X1) - np.log(X2)  # v1 Amplitude # TODO
X_LR = np.log(X1 ** 2) - np.log(X2 ** 2)  # v2 Intensity # TODO
X_LR_VV = np.ndarray.flatten(X_LR[:, :, 0])
X_LR_VH = np.ndarray.flatten(X_LR[:, :, 1])
