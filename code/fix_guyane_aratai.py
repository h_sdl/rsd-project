import os

import numpy as np

from data import writeGTIFF, readGTIFF
from settings import GRD_FOLDER

if __name__ == "__main__":
    """
    Simple script to crop part black part
    of the "GuyaneAratai" images of the dataset.
    
    New images are saved in a "GuyaneAratai_fixed" folder.
    
    """
    place = "GuyaneAratai"

    images = sorted(os.listdir(GRD_FOLDER / place))

    # The name of the place sometimes changes
    prefix = images[0].split("_")[0]

    os.makedirs(GRD_FOLDER / "GuyaneAratai_fixed", exist_ok=True)

    # Extremal dates
    for image in images:
        date = image.split("_")[-1].replace(".tif", "")

        path_vv = GRD_FOLDER / place / f"{prefix}_vv_{date}.tif"
        path_vh = GRD_FOLDER / place / f"{prefix}_vh_{date}.tif"

        X = np.concatenate([readGTIFF(path_vv), readGTIFF(path_vh)], axis=2)

        # Best crop for those images
        X = X[:1275, :, :]

        writeGTIFF(
            X[:, :, 0],
            str(path_vv).replace(place, f"{place}_fixed"),
            copy_metadata_from=path_vv,
        )
        writeGTIFF(
            X[:, :, 1],
            str(path_vh).replace(place, f"{place}_fixed"),
            copy_metadata_from=path_vh,
        )
