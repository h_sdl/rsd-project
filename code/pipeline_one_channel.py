#!/usr/bin/env python
# coding: utf-8
import logging
import os

import matplotlib.pyplot as plt
import numpy as np
from matplotlib import colors

from data import load_grd_data
from method import nakagami_mixture_em, estimate_nonchange_nakagami_parameters
from plots import visusar
from settings import (
    eps,
    eq_nb_looks_GRD,
    DEFAULT_FIG_SIZE,
    ONE_POL_FIGS_FOLDER,
    FIGS_FOLDER,
)
from stats import nakagami

logging.basicConfig(level=logging.INFO)

"""
Simple adaptation for a mono-channel setting
* Adapted pipeline
* Adapted results_plotting
* Script to run on given GRD data 
"""


def plot_results_one_channel(
    X1_VV,
    X2_VV,
    rho,
    date1,
    date2,
    P,
    m_star,
    s_star,
    pixel_class,
    save_path=None,
    place="",
    channel="",
):
    """
    Plot images between two dates and associated results

    :param place:
    :param X1_VV: (h, w) np.ndarray
    :param X2_VV: (h, w) np.ndarray
    :param rho: (h × w) np.ndarray
    :param date1: first date string
    :param date2: second date string
    :param P: prior of the distribution
    :param m_star: the shape parameters for the Nakagami distributions
    :param s_star: the scale parameters for the Nakagami distributions
    :param pixel_class: class of pixels
    :param save_path: where to save
    :param place: name of the place depicted by the
    :param channel: name of channel used by the pipeline
    :return:
    """
    h, w = X1_VV.shape

    plt.figure(figsize=DEFAULT_FIG_SIZE)

    im_to_plot = np.log(1 + X2_VV / X1_VV)
    plt.title(f"Log ratio on {place} between {date1} and {date2} on {channel} channel")

    plt.imshow(im_to_plot, interpolation="nearest")
    plt.set_cmap("gray")

    plt.savefig(save_path)

    plt.show()

    n_classes = int(pixel_class.max() + 1)

    fig = plt.figure(constrained_layout=True, figsize=DEFAULT_FIG_SIZE)

    gs = fig.add_gridspec(ncols=2, nrows=2)

    # Original images
    x1d1 = fig.add_subplot(gs[0, 0])
    visusar(X1_VV)
    x1d1.set_title(f"{place} {date1} {channel} channel")

    x2d1 = fig.add_subplot(gs[1, 0])
    visusar(X2_VV)
    x2d1.set_title(f"{place} {date2} {channel} channel")

    # Polarimetric changes vectors
    rho_ax = fig.add_subplot(gs[0, 1])
    r = np.arange(rho.min(), rho.max(), 0.01) + eps

    # omega_{nc}
    nc = np.array([P[0] * nakagami(x, m_star[0], s_star[0]) for x in r])
    # omega_c
    c = np.array([P[1] * nakagami(x, m_star[1], s_star[1]) for x in r])

    plt.plot(
        r,
        nc,
        label=f"Unchanged Nagakami({m_star[0]:.2f}, {s_star[0]:.2f}) ; prior={P[0]:.2f}",
    )
    plt.plot(
        r,
        c,
        label=f"Changed Nagakami({m_star[1]:.2f}, {s_star[1]:.2f}) ; prior={P[1]:.2f}",
    )
    plt.hist(
        rho,
        density=True,
        bins=int(np.sqrt(np.size(rho))),
        alpha=0.5,
        label="Empirical distribution",
    )
    plt.plot(r, nc + c, label=f"Full model (sum)")
    plt.legend()
    plt.title(f"{place} Rho distributions")

    # Pixel classification
    pixels_ax = fig.add_subplot(gs[1, 1])

    pixel_class_rect = pixel_class.reshape((h, w))
    cols = ["y", "b", "g", "r", "c", "m", "y", "k"]
    pixels_ax.imshow(pixel_class_rect, cmap=colors.ListedColormap(cols[:n_classes]))
    pixels_ax.set_title("Pixel classification")

    if save_path is not None:
        plt.savefig(save_path)

    plt.show()


def pipeline_one_channel(
    X1: np.ndarray,
    X2: np.ndarray,
    date1: str,
    date2: str,
    s_c: float = 5,
    m_c: float = 5,
    P_nc=0.9,
    fixed_nc_distribution=False,
    m_nc: float = None,
    s_nc: float = None,
    channel="VV",
    place="",
):
    """
    Perform the change detection and classification between two
    one-pol images as described in [2].

    First classify the pixels as associated to change or not then
    classify the changed pixels in `n_class_changes` subclasses.

    Log-ratios, distributions and results are shown and saved.

    [2] Advanced Methods for Change Detection in Multi-polarization and
    Very-High Resolution Multitemporal SAR Images.
    Davide Pirrone. PhD thesis, International Doctorate School in
    Information and Communication Technologies - University of Trento, 1 2019.


    :param X1: first image, one channel only
    :param X2: second image, one channel only
    :param date1: first date
    :param date2: second date
    :param fixed_nc_distribution: fix the theoretical distribution of non-change
    :param P_nc: the prior to use for the non-change distribution
    :param m_nc: the theoretical shape parameter of the non-change distribution
    :param s_nc: the theoretical spread parameter of the non-change distribution
    :param m_c: the initial shape parameter of the change distribution
    :param s_c: the initial spread parameter of the change distribution
    :param channel: the channel used
    :param place: name associated to the place depicted by the images
    :return:
    """
    logging.info(f"   → Image 1 date: {date1}")
    logging.info(f"   → Image 2 date: {date2}")

    # Compute Polarimetric Change Vector
    X_LR = np.ndarray.flatten(np.log(X2 ** 2 + eps) - np.log(X1 ** 2 + eps))

    # Polar coordinates
    rho = np.sqrt(X_LR ** 2)

    # Binary clustering
    logging.info(f"   → Binary Classification")
    P_binary, m, s, p_binary = nakagami_mixture_em(
        rho,
        P0=np.array([P_nc, 1 - P_nc]),
        m0=np.array([m_nc, m_c]),
        s0=np.array([s_nc, s_c]),
        fixed_nc_distribution=fixed_nc_distribution,
        m_nc=m_nc,
        s_nc=s_nc,
    )
    P_binary_star, m_star, s_star = P_binary[:, -1], m[:, -1], s[:, -1]

    # Multi-class classification
    class_indices = np.zeros(X_LR.shape)

    # We apply Bayes rules here
    changed_pixels_index = p_binary[:, 0] <= 0.5
    if changed_pixels_index.sum() == 0:
        logging.info(f"   → No change detected!")

    # 0: no change, 1: change
    class_indices[changed_pixels_index] = 1

    logging.info(f"   → Plot Results")
    save_path = ONE_POL_FIGS_FOLDER / f"{place}_{channel}_res.png"

    plot_results_one_channel(
        X1,
        X2,
        rho,
        date1,
        date2,
        P_binary_star,
        m_star,
        s_star,
        class_indices,
        save_path=save_path,
        place=place,
        channel=channel,
    )

    plt.close("all")


if __name__ == "__main__":

    grd_places = [
        "GuyaneAratai",
        "Sajnam",
        "DesMoines",
        "Bruz",
        "Gambie",
        "Angers",
        "Toulouse",
        "MadridIowa",
        "Montbel",
        "Toulouse",
        "MadridIowa",
        "Montbel",
        "GuyaneFortune",
        "Redon",
    ]

    m_GRD, s_GRD, stdm_GRD, stds_GRD = estimate_nonchange_nakagami_parameters(
        eq_nb_looks_GRD
    )
    logging.info("→ Computed non-change Nakagami parameters for GRD images")
    logging.info(
        f" GRD : L = {eq_nb_looks_GRD}; m = {m_GRD:.2f} ± {stdm_GRD:.2f}; s={s_GRD:.2f} ± {stdm_GRD:.2f}"
    )

    os.makedirs(FIGS_FOLDER, exist_ok=True)
    os.makedirs(ONE_POL_FIGS_FOLDER, exist_ok=True)

    load_func_places_grd = [
        (place, load_grd_data, m_GRD, s_GRD) for place in grd_places
    ]

    load_func_places = load_func_places_grd

    for place, load_data_func, m, s in load_func_places:
        logging.info(f"→ Run pipeline on {place}")

        logging.info(f"   → Load data")
        X1, X2, date1, date2 = load_data_func(place)

        # We choose the VV channel, but we could have chosen the VH instead
        X1_VV = X1[:, :, 0]
        X2_VV = X2[:, :, 0]

        pipeline_one_channel(
            X1_VV, X2_VV, date1, date2, m_nc=m, s_nc=s, channel="VV", place=place
        )
