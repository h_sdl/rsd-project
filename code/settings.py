#!/usr/bin/env python
# coding: utf-8

import pandas as pd
from pathlib import Path

"""
Settings of the project.
"""

# FOLDERS
ROOT = Path(__file__).parents[1]

# Data given
DATA_FOLDER = ROOT / "data"
GRD_FOLDER = Path(DATA_FOLDER) / "GRD"
SLC_FOLDER = Path(DATA_FOLDER) / "SLC"

FIGS_FOLDER = ROOT / "figs"
GRD_FIGS_FOLDER = FIGS_FOLDER / "grd"
SLC_FIGS_FOLDER = FIGS_FOLDER / "slc"
ONE_POL_FIGS_FOLDER = FIGS_FOLDER / "one_pol"

# Matplotlib figure size
DEFAULT_FIG_SIZE = (30, 15)

# To prevent infinite values or nan's
eps = 1e-8

data_parameters = pd.read_csv(Path(DATA_FOLDER) / "parameters.csv", sep=";")

# Equivalent number of looks of GRD images # DO NOT CHANGE ! (fixed by ESA)
eq_nb_looks_GRD = 4.5

# Number of looks used for SLC # CAN BE CHANGED
nb_looks_SLC = 9
